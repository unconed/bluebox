import React, { LC, use, hot, into, useFiber, useOne, useResource } from '@use-gpu/live';

import { Router, Routes } from '@use-gpu/workbench';
import { AutoCanvas, WebGPU } from '@use-gpu/webgpu';
import { HTML } from '@use-gpu/react';
import { FontLoader } from '@use-gpu/workbench';
import { UseInspect } from '@use-gpu/inspect';
import { inspectGPU } from '@use-gpu/inspect-gpu';
import mapValues from 'lodash/mapValues';

import '@use-gpu/inspect/theme.css';
import NOTO_SEQUENCES from './noto-emoji.json';

import { Gen } from './gen/Gen';
import { Reorder1, Reorder2, Reorder3, Reorder4 } from './graph/Reorder';
import { SamplingRun1 } from './graph/SamplingRun1';
import { SamplingRun2 } from './graph/SamplingRun2';
import { SamplingRun3 } from './graph/SamplingRun3';
import { SamplingRun4 } from './graph/SamplingRun4';
import { SamplingRun5 } from './graph/SamplingRun5';
import { SamplingRun6 } from './graph/SamplingRun6';
import { Gradient1 } from './graph/Gradient1';
import { Gradient2 } from './graph/Gradient2';
import { Gradient3 } from './graph/Gradient3';

import { FALLBACK_MESSAGE, Video, Image } from './html/Fallback';

export const base = location.host === 'localhost:4000' || location.host === 'acko.net' ? "/files/bluebox/" : "/";

//const inspect = location.hostname === 'localhost';
const getNotoEmojiURL = (name: string) => `${base}fonts/emoji/emoji_u${name}.png`;

const ROUTES = {
  '/graph/reorder1': {element: <Reorder1 />},
  '/graph/reorder2': {element: <Reorder2 />},
  '/graph/reorder3': {element: <Reorder3 />},
  '/graph/reorder4': {element: <Reorder4 />},
  '/graph/gradient1': {element: <Gradient1 />},
  '/graph/gradient2': {element: <Gradient2 />},
  '/graph/gradient3': {element: <Gradient3 />},
  '/graph/sampling1': {element: <SamplingRun1 />},
  '/graph/sampling2': {element: <SamplingRun2 />},
  '/graph/sampling3': {element: <SamplingRun3 />},
  '/graph/sampling4p': {element: <SamplingRun4 index={0} />},
  '/graph/sampling4m': {element: <SamplingRun4 index={1} />},
  '/graph/sampling4n': {element: <SamplingRun4 index={2} />},
  '/graph/sampling5a': {element: <SamplingRun5 index={0} />},
  '/graph/sampling5b': {element: <SamplingRun5 index={1} />},
  '/graph/sampling5c': {element: <SamplingRun5 index={2} />},
  '/graph/sampling5d': {element: <SamplingRun5 index={3} />},
  '/graph/sampling6': {element: <SamplingRun6 />},
  '/gen': {element: <Gen />},
  '/': {element: <Gen />, exact: true},
};

const FALLBACK_ROUTES = mapValues(ROUTES, (r) => ({
  ...r,
  element: r.fallback ?? use(HTML, {children: FALLBACK_MESSAGE("Your browser does not support WebGPU")}),
}));

const BLACK = [0,0,0,1];

export const App: LC = hot(() => {
  const fiber = useFiber();

  const root = document.querySelector('#gpu-box')!;
  const inner = document.querySelector('#gpu-box .canvas')!;

  const fonts = useOne(() => [
    {
      family: 'Lato',
      weight: 400,
      style: 'normal',
      src: base + 'fonts/Lato-Regular.ttf',
    },
    {
      family: 'Lato',
      weight: 500,
      style: 'normal',
      src: base + 'fonts/Lato-Bold.ttf',
    },
    {
      family: 'Lato',
      weight: 900,
      style: 'normal',
      src: base + 'fonts/Lato-Black.ttf',
    },
    {
      family: 'Noto Emoji',
      weight: 400,
      style: 'normal',
      lazy: {
        sequences: NOTO_SEQUENCES,
        fetch: (index: number) => {
          // name = "XXXX_XXXX_XXXX" where X = codepoint in hex
          const seq = NOTO_SEQUENCES[index];
          const codepoints = [...seq].map(s => s.codePointAt(0)!);
          const name = codepoints.map(i => i.toString(16)).join('_');
          return getNotoEmojiURL(name);
        },
      },
    }
  ]);
  
  const view = (
    <Router base={base} hash>
      <WebGPU
        fallback={(error: Error) =>
            <Routes routes={FALLBACK_ROUTES} />
        }
      >
        <AutoCanvas
          iframe
          backgroundColor={BLACK}
          selector={'#gpu-box .canvas'}
          samples={4}
        >
          <FontLoader fonts={fonts}>
            <Routes routes={ROUTES} />
          </FontLoader>
        </AutoCanvas>
      </WebGPU>
    </Router>
  );

  /*
  useResource((dispose) => {
    const canvas = document.querySelector('canvas');
    const f = (e) => {
      e.stopPropagation();
    };
    document.body.addEventListener('wheel', f, {capture: true});
    dispose(() => document.body.removeEventListener('wheel', f, {capture: true}));
  })
  */
  
  const inspect = true;

  if (inspect) {
    return (
      <UseInspect
        fiber={fiber}
        container={root}
        extensions={[inspectGPU]}
        appearance={{toolbar: true}}
      >
        {view}
      </UseInspect>
    );
  }

  //return view;
}, module);

App.displayName = 'App';
