const SCALE = 1.0;

@export fn from64(x: vec2<u32>) -> f32 {
  return (f32(x.x) + f32(x.y) * (1.0/f32(0x100000000))) * SCALE;
};

@export fn to64(x: f32) -> vec2<u32> {
  let xx = x / SCALE;
  let i = floor(xx);
  let d = (xx - i) * f32(0x100000000);

  return vec2<u32>(u32(i), u32(d));
};

@export fn add64(a: vec2<u32>, b: vec2<u32>) -> vec2<u32> {

  let d = a.y + b.y;
  let o = select(0u, 1u, d < min(a.y, b.y));
  let i = a.x + b.x + o;

  return vec2<u32>(u32(i), u32(d));
};
