use './array'::{ sizeToModulus3, packIndex3 };

@link fn getSize() -> vec3<u32>;

@link fn getTarget(x: f32, y: f32, z: f32) -> f32;

@link var<storage, read_write> targetBuffer: array<vec2<f32>>;

@compute @workgroup_size(4, 4, 4)
@export fn swapTarget(
  @builtin(global_invocation_id) globalId: vec3<u32>,
) {
  let size = getSize();
  if (any(globalId.xyz >= size)) { return; }
  let fragmentId = globalId.xyz;

  let modulus = sizeToModulus3(size);
  let index = packIndex3(fragmentId, modulus);

  let xyz = 2.0 * vec3<f32>(fragmentId) / vec3<f32>(size);
  let f = min(xyz, 2.0 - xyz);

  let goal = getTarget(f.x, f.y, f.z);

  targetBuffer[index] = vec2<f32>(goal, 0.0);
}
