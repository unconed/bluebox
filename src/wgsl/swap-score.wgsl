use './array'::{ sizeToModulus3, unpackIndex3 };
//use './f64'::{ from64, to64, add64 };

@link fn getSize() -> vec3<u32>;
@link fn getIndex() -> u32;

@link fn getTarget(x: f32, y: f32, z: f32) -> f32;

@link var<storage> dataBuffer: array<vec2<f32>>;
@link var<storage, read_write> errorBuffer: array<f32>;

@link var<storage> meanBuffer: array<f32>;
@link var<storage, read_write> scoreBuffer: array<f32>;

var<workgroup> accumBuffer: array<f32, 256>;
//var<workgroup> goalBuffer: array<f32, 256>;
//var<workgroup> accumBuffer: array<vec2<u32>, 256>;
//var<workgroup> goalBuffer: array<vec2<u32>, 256>;

@compute @workgroup_size(256, 1, 1)
@export fn swapScore(
  @builtin(global_invocation_id) globalId: vec3<u32>,
) {
  let size = getSize();
  let modulus = sizeToModulus3(size);

  let count = size.x * size.y * size.z;
  let steps = count / 256;

  let thread = globalId.x;
  let imean = meanBuffer[0];
  let ixyz = 2.0 / vec3<f32>(size);

  var x = 1.0;

  for (var i = 0u; i < steps; i++) {
    let index = thread + i * 256;
    if (index < count) {
      let xyz = vec3<f32>(unpackIndex3(index, modulus)) * ixyz;
      let f = min(xyz, 2.0 - xyz);

      let spectrum = length(dataBuffer[index]) * imean;
      let goal = getTarget(f.x, f.y, f.z);

      let error = sqr(goal - spectrum);

      accumBuffer[thread] += error;
      //goalBuffer[thread] += goal;
      //accumBuffer[thread] = add64(accumBuffer[thread], to64(error));
      //goalBuffer[thread] = add64(goalBuffer[thread], to64(goa));

      if (SCORE_ONLY) { errorBuffer[index] = error; }
    }
  }

  workgroupBarrier();

  if (thread > 0) { return; }

  var accumValue = 0.0;
  //var goalValue = 0.0;
  //var accumValue = vec2<u32>(0);
  //var goalValue = vec2<u32>(0);
  for (var i = 0; i < 256; i++) {
    accumValue += accumBuffer[i];
    //goalValue += goalBuffer[i];
    //accumValue = add64(accumValue, accumBuffer[i]);
    //goalValue = add64(goalValue, goalBuffer[i]);
  }

  let q = accumValue;
  //let g = goalValue;
  //let q = from64(accumValue);
  //let g = from64(goalValue);

  let v = sqrt(q / f32(count));
  if (!SCORE_COMPARE) {
    let i = getIndex();
    scoreBuffer[i] = v;
  }
  else {
    scoreBuffer[0] = v;
  }
}

fn sqr(x: f32) -> f32 { return x * x; }
