use './array'::{ sizeToModulus3, packIndex3 };

@link fn getSize() -> vec3<u32>;

@link fn getTarget(x: f32, y: f32, z: f32) -> f32;

@link var<storage, read_write> dataBuffer: array<vec2<f32>>;
@link var<storage> meanBuffer: array<f32>;

@compute @workgroup_size(4, 4, 4)
@export fn swapGradient(
  @builtin(global_invocation_id) globalId: vec3<u32>,
) {
  let size = getSize();
  if (any(globalId.xyz >= size)) { return; }
  let fragmentId = globalId.xyz;

  let imean = meanBuffer[0];

  let modulus = sizeToModulus3(size);
  let index = packIndex3(fragmentId, modulus);

  let xyz = 2.0 * vec3<f32>(fragmentId) / vec3<f32>(size);
  let f = min(xyz, 2.0 - xyz);

  let spectrum = dataBuffer[index] * imean;
  let goal = getTarget(f.x, f.y, f.z);

  let gradient = select(vec2<f32>(0.0), goal * normalize(spectrum) - spectrum, length(spectrum) > 0.0);
  dataBuffer[index] = gradient;
}

fn sqr(x: f32) -> f32 { return x * x; }
