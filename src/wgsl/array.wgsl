@export const IS_PACKED = false;

@export fn sizeToModulus3(size: vec3<u32>) -> vec3<u32> {
  let s = size.x * size.y;
  if (IS_PACKED) {
    return vec3<u32>(size.x / 4, s / 16, 0xffffffffu);

    /*
    return vec3<u32>(size.x / 2, s / 4, 0xffffffffu);
    */
  }
  else {
    return vec3<u32>(size.x, s, 0xffffffffu);
  }
}

@export fn packIndex3(v: vec3<u32>, modulus: vec3<u32>) -> u32 {
  if (IS_PACKED) {
    let vbase = v / 4;
    let vbit = v & vec3<u32>(3);

    let ibase = dot(vbase, vec3<u32>(1u, modulus.xy)) << 6u;
    let offset = dot(vbit, vec3<u32>(1u, 4u, 16u));

    /*
    let vbase = v / 2;
    let vbit = v & vec3<u32>(1);

    let ibase = dot(vbase, vec3<u32>(1u, modulus.xy)) << 3u;
    let offset = dot(vbit, vec3<u32>(1u, 2u, 4u));
    */

    return ibase + offset;
  }
  else {
    return dot(v, vec3<u32>(1u, modulus.xy));
  }
}

@export fn unpackIndex3(i: u32, modulus: vec3<u32>) -> vec3<u32> {
  if (IS_PACKED) {
    let ibase = i >> 6u;
    let xyz = (vec3<u32>(i) & vec3<u32>(3u, 12u, 48u)) >> vec3<u32>(0u, 2u, 4u);
    return (ibase % modulus) / vec3<u32>(1u, modulus.xy) * 4u + xyz;

    /*
    let ibase = i >> 3u;
    let xyz = (vec3<u32>(i) & vec3<u32>(1u, 2u, 4u)) >> vec3<u32>(0u, 1u, 2u);
    return (ibase % modulus) / vec3<u32>(1u, modulus.xy) * 2u + xyz;
    */
  }
  else {
    return (i % modulus) / vec3<u32>(1u, modulus.xy);
  }
}

@export fn wrapIndex3(index: vec3<i32>, size: vec3<u32>) -> vec3<u32> {
  let s = vec3<i32>(size);
  var signedIndex = index;
  signedIndex = select(signedIndex, signedIndex + s, signedIndex < vec3<i32>(0));
  signedIndex = select(signedIndex, signedIndex - s, signedIndex >= s);
  return vec3<u32>(signedIndex);
}
