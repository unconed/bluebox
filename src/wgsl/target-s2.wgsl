@link fn getNorm() -> f32;

@link fn getRange() -> vec2<f32>;
@link fn getSpace(t: f32, r: vec2<f32>) -> f32;

@export fn getTargetS2(x: f32, y: f32, z: f32) -> f32 {
  let s = length(vec2<f32>(x, y));
  return getSpace(s, getRange()) * getNorm();
};
