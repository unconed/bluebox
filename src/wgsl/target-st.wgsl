@link fn getNorm() -> f32;

@link fn getSpaceRange() -> vec2<f32>;
@link fn getSpace(t: f32, r: vec2<f32>) -> f32;

@link fn getTimeRange() -> vec2<f32>;
@link fn getTime(t: f32, r: vec2<f32>) -> f32;

@export fn getTargetST(x: f32, y: f32, z: f32) -> f32 {
  let s = length(vec2<f32>(x, y));
  let t = z;
  return getSpace(s, getSpaceRange()) * getTime(t, getTimeRange()) * getNorm();
};
