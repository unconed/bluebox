use './array'::{ sizeToModulus3, packIndex3, IS_PACKED };

const TAU = 6.2831853072;

@link fn getSize() -> vec3<u32>;
@link fn getAxis() -> u32;

@link var<storage, read_write> fftBuffer: array<vec2<f32>>;

@compute @workgroup_size(8, 8, 1)
@export fn fftApply(
  @builtin(global_invocation_id) globalId: vec3<u32>,
) {
  let size = getSize();
  let axis = getAxis();

  let modulus = sizeToModulus3(size);

  var dispatchSize: vec2<u32>;
  var length: u32;

  var base3: vec3<u32>;
  var stride3: vec3<u32>;

  var base: u32;
  var stride: u32;

  if (axis == 1) {
    dispatchSize = size.yz;
    base3 = vec3<u32>(0, globalId.xy);
    stride3 = vec3<u32>(1, 0, 0);
    base = packIndex3(base3, modulus);
    stride = 1;
    length = size.x;
  }
  else if (axis == 2) {
    dispatchSize = size.xz;
    base3 = vec3<u32>(globalId.x, 0, globalId.y);
    stride3 = vec3<u32>(0, 1, 0);
    base = packIndex3(base3, modulus);
    stride = modulus.x;
    length = size.y;
  }
  else if (axis == 3) {
    dispatchSize = size.xy;
    base3 = vec3<u32>(globalId.xy, 0);
    stride3 = vec3<u32>(0, 0, 1);
    base = packIndex3(base3, modulus);
    stride = modulus.y;
    length = size.z;
  }

  if (any(globalId.xy >= dispatchSize)) { return; }

  let sign = select(-1, 1, INVERSE);
  let levels = firstLeadingBit(length);
  for (var i = 0u; i < levels; i++) {
    let n = 1u << i;
    let l = length >> i;
    for (var j = 0u; j < n; j++) {
      if (IS_PACKED) {
        let start = base3 + j * l * stride3;
        fftPassPacked(start, stride3, modulus, l, sign);
      }
      else {
        let start = base + j * l * stride;
        fftPass(start, stride, l, sign);
      }
    }
  }

  if (IS_PACKED) {
    let shift = 32 - levels;
    for (var i = 0u; i < length; i++) {
      let j = reverseBits(i) >> shift;
      if (i < j) {
        let a = packIndex3(base3 + i * stride3, modulus);
        let b = packIndex3(base3 + j * stride3, modulus);

        let x = fftBuffer[a];
        let y = fftBuffer[b];
        fftBuffer[a] = y;
        fftBuffer[b] = x;
      }
    }
  }
  else {
    let shift = 32 - levels;
    for (var i = 0u; i < length; i++) {
      let j = reverseBits(i) >> shift;
      if (i < j) {
        let a = base + i * stride;
        let b = base + j * stride;

        let x = fftBuffer[a];
        let y = fftBuffer[b];
        fftBuffer[a] = y;
        fftBuffer[b] = x;
      }
    }
  }
}

fn fftPass(base: u32, stride: u32, length: u32, sign: i32) {
  let n = i32(length);
  let half = n / 2;
  let mid = base + length * stride / 2;

  for (var i = 0; i < half; i++) {
    let is = u32(i) * stride;

    let a = base + is;
    let b = mid + is;

    let x = fftBuffer[a];
    let y = fftBuffer[b];

    fftBuffer[a] = x + y;
    fftBuffer[b] = mulComplex(x - y, rootOfUnity(n, i * sign));
  }
}

fn fftPassPacked(base: vec3<u32>, stride: vec3<u32>, modulus: vec3<u32>, length: u32, sign: i32) {
  let n = i32(length);
  let half = n / 2;
  let mid = base + length * stride / 2;

  for (var i = 0; i < half; i++) {
    let is = u32(i) * stride;

    let a = packIndex3(base + is, modulus);
    let b = packIndex3(mid + is, modulus);

    let x = fftBuffer[a];
    let y = fftBuffer[b];

    fftBuffer[a] = x + y;
    fftBuffer[b] = mulComplex(x - y, rootOfUnity(n, i * sign));
  }
}

fn mulComplex(a: vec2<f32>, b: vec2<f32>) -> vec2<f32> {
  return vec2<f32>(a.x * b.x - a.y * b.y, a.x * b.y + a.y * b.x);
}


fn rootOfUnity(n: i32, x: i32) -> vec2<f32> {
  if (x == 0) { return vec2<f32>(1.0, 0.0); }

  if (n % 2 == 0) {
    if (x == n / 2) { return vec2<f32>(-1.0, 0.0); }
    if (n % 4 == 0) {
      if (x == n / 4) { return vec2<f32>(0.0, 1.0); }
      if (x == n * 3 / 4) { return vec2<f32>(0.0, -1.0); }
    }
  }

  let angle = TAU * f32(x) / f32(n);
  return vec2<f32>(cos(angle), sin(angle));
}
