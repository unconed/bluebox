use './array'::{ sizeToModulus3, packIndex3 };

const MAX_TRIES = 128;
const PHI2_1 = 0.7548776662466927;
const PHI2_2 = 0.5698402909980532;

@link var<storage, read_write> pixelBuffer: array<f32>;
@link var<storage> gradientBuffer: array<vec2<f32>>;
@link var<storage, read_write> countBuffer: array<u32>;

@link fn getBatch() -> u32;

@link fn getIter() -> u32;
@link fn getSize() -> f32;
@link fn getSeed1() -> f32;
@link fn getSeed2() -> f32;

@link var<storage> scoreBuffer: array<f32>;
@link var<storage, read_write> swapBuffer: array<u32>;

@compute @workgroup_size(1, 1, 1)
@export fn swapApply() {

  var a = swapBuffer[0];
  var b = swapBuffer[1];

  let size = getSize();
  let seed1 = getSeed1();
  let seed2 = getSeed2();

  if (SWAP_UNDO) {
    let i = countBuffer[0];
    if (i == getBatch()) {
      countBuffer[2] = countBuffer[1];
      countBuffer[1] = 0;
      countBuffer[0] = 0;
    }
    countBuffer[0]++;

    if (SWAP_SPARSE) {
      let q1a = scoreBuffer[0];
      let q2a = scoreBuffer[1];
      let q1b = scoreBuffer[2];
      let q2b = scoreBuffer[3];

      let current = q1b + q2b;
      let last = q1a + q2a;

      if (current < last) {
        countBuffer[1]++;
        return;
      }
    }
    else {
      let last = scoreBuffer[1];
      let current = scoreBuffer[0];

      if (current < last) {
        scoreBuffer[1] = scoreBuffer[0];
        countBuffer[1]++;
        return;
      }
      else if (last == 0) {
        scoreBuffer[1] = scoreBuffer[0];
      }
    }
  }
  else if (SWAP_GENERATE) {
    if (USE_GRADIENT) {
      for (var i = 0; i < MAX_TRIES; i++) {
        a = getRand(seed1, size, i, PHI2_1);
        b = getRand(seed2, size, i, PHI2_2);

        let v1 = pixelBuffer[a];
        let v2 = pixelBuffer[b];

        let dcdn1 = gradientBuffer[a].x;
        let dcdn2 = gradientBuffer[b].x;
        let dcdn = dcdn1 - dcdn2;

        if ((v2 - v1) * dcdn < 0) { continue; }
        break;
      }
    }
    else {
      a = getRand(seed1, size, 0, PHI2_1);
      b = getRand(seed2, size, 0, PHI2_2);
    }
  }

  if (SWAP_WARMUP && getIter() < 2) {
    return;
  }

  if (SWAP_APPLY) {
    let v1 = pixelBuffer[a];
    let v2 = pixelBuffer[b];

    pixelBuffer[a] = v2;
    pixelBuffer[b] = v1;
  }

  swapBuffer[0] = a;
  swapBuffer[1] = b;
}

fn getRand(seed: f32, size: f32, i: i32, offset: f32) -> u32 {
  return u32(fract(seed + f32(i) * offset) * size);
};
