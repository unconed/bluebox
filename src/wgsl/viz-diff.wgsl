@link fn getGainA() -> f32;
@link fn getGainB() -> f32;

@link fn getA(i: u32) -> vec2<f32>;
@link fn getB(i: u32) -> vec2<f32>;

@export fn getDiff(i: u32) -> vec2<f32> {
  return getA(i) * getGainA() - getB(i) * getGainB();
}
