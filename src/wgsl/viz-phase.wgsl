use './array'::{ packIndex3, sizeToModulus3 };

@link fn getDataSize() -> vec3<u32>;
@link fn getData(i: u32) -> vec2<f32>;
@optional @link fn getLayer() -> u32 { return 0; };
@optional @link fn getMin() -> f32 { return 0.0; };
@optional @link fn getMax() -> f32 { return 1.0; };
@optional @link fn getShift() -> f32 { return 0.0; };
@optional @link fn getColorize() -> u32 { return 0; };

@export fn visualizePhase(uv: vec2<f32>) -> vec4<f32> {
  let size = getDataSize();
  let layer = getLayer();

  let minValue = getMin();
  let maxValue = getMax();
  let shift = getShift();
  let colorize = getColorize();

  let uva = vec2<u32>(fract(uv + shift) * vec2<f32>(size.xy));
  let modulus = sizeToModulus3(size);
  let index = packIndex3(vec3<u32>(uva, layer), modulus);

  let value = getData(index);
  let mag = select(0.0, atan2(value.y, value.x), length(value) > 0.0);
  let level = (mag - minValue) / (maxValue - minValue);

  if (colorize > 0) {
    let h = level + 0.16666;
    let hr = fract(h) * 3.0;
    let hg = fract(h - 1./3.) * 3.0;
    let hb = fract(h - 2./3.) * 3.0;

    let r = clamp(min(hr, 2.0 - hr), 0.0, 1.0);
    let g = clamp(min(hg, 2.0 - hg), 0.0, 1.0);
    let b = clamp(min(hb, 2.0 - hb), 0.0, 1.0);

    return vec4<f32>(r, g, b, 1.0);
  }

  return vec4<f32>(vec3<f32>(level), 1.0);
}
