@link fn getResolution() -> vec2<f32>;
@link fn getDataSize() -> vec2<f32>;
@link fn getData(uv: vec2<f32>) -> vec4<f32>;

@export fn visualizeAA(uv: vec2<f32>) -> vec4<f32> {
  let dx = dpdx(uv);
  let dy = dpdy(uv);
  let df = (length(dx) + length(dy)) / 2.0;

  let r = getResolution();
  let dt = length(r);

  let xy = uv * getDataSize();
  let scale = dt / df / 2.0;

  let ixy = floor(xy);

  var dxy: vec2<f32>;
  if (df > dt) {
    dxy = vec2<f32>(0.5);
  }
  else {
    let dxys = xy - ixy;
    dxy = clamp((dxys - .5) * scale + .5, vec2<f32>(0.0), vec2<f32>(1.0));
  }

  let tl = getData(ixy * r);
  let tr = getData(ixy * r + vec2<f32>(r.x, 0.0));
  let bl = getData(ixy * r + vec2<f32>(0.0, r.y));
  let br = getData(ixy * r + vec2<f32>(r));

  let a1 = mix(tl, tr, dxy.x);
  let a2 = mix(bl, br, dxy.x);

  let v = mix(a1, a2, dxy.y);
  return v;
};
