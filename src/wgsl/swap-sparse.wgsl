use './array'::{ sizeToModulus3, packIndex3, unpackIndex3, wrapIndex3 };

@link fn getSource(i: u32) -> vec2<f32>;

@link fn getSize() -> vec3<u32>;
@link fn getBlockSize() -> vec3<u32>;
@link fn getBlockOffset() -> vec3<i32>;
@link fn getIndex() -> u32;

@link var<storage, read_write> swapBuffer: array<u32>;

fn getWindow(t: f32) -> f32 { return 1.0 - (3.0 - 2.0*t)*t*t; }

@export fn swapSparse(i: u32) -> vec2<f32> {
  let size = getSize();
  let blockSize = getBlockSize();
  let blockOffset = getBlockOffset();
  let index = getIndex();

  let ms = sizeToModulus3(size);
  let mbs = sizeToModulus3(blockSize);

  let base = swapBuffer[index];
  var xyz = vec3<i32>(unpackIndex3(base, ms));
  xyz -= blockOffset;

  var dxyz = vec3<i32>(unpackIndex3(i, mbs));

  var wrap = wrapIndex3(xyz + dxyz, size);
  let packed = packIndex3(wrap, ms);

  let delta = select(vec3<f32>(0.0), vec3<f32>(dxyz) / vec3<f32>(blockSize) * 2.0 - 1.0, blockSize != vec3<u32>(1));
  let f = min(1.0, length(delta));
  let w = 1.0 - (3.0 - 2.0*f)*f*f;

  let d = getSource(packed);
  return w * d;
}
