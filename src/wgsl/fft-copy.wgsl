use './array'::{ sizeToModulus3, packIndex3 };

@link fn getSize() -> vec3<u32>;

@link var<storage, read_write> fftBuffer: array<vec2<f32>>;
@link fn getSource(i: u32) -> vec2<f32>;

@compute @workgroup_size(64, 1, 1)
@export fn fftCopy(
  @builtin(global_invocation_id) globalId: vec3<u32>,
) {
  let size = getSize();
  if (any(globalId.xyz >= size)) { return; }
  let fragmentId = globalId.xyz;

  let modulus = sizeToModulus3(size);
  let index = packIndex3(fragmentId, modulus);

  fftBuffer[index] = getSource(index);
}
