use './array'::{ sizeToModulus3, packIndex3 };
//use './f64'::{ from64, to64, add64 };

@link fn getSize() -> vec3<u32>;

@link var<storage> dataBuffer: array<vec2<f32>>;

@link var<storage, read_write> meanBuffer: array<f32>;

var<workgroup> accumBuffer: array<f32, 256>;
//var<workgroup> accumBuffer: array<vec2<u32>, 256>;

@compute @workgroup_size(256, 1, 1)
@export fn swapMean(
  @builtin(global_invocation_id) globalId: vec3<u32>,
) {
  let size = getSize();
  let modulus = sizeToModulus3(size);

  let count = i32(size.x * size.y * size.z);
  let steps = count / 256;

  let thread = i32(globalId.x);

  for (var i = 0; i < steps; i++) {
    let index = thread + i * 256;
    if (index < count) {
      let f = length(dataBuffer[index]);
      accumBuffer[thread] += f;
      //accumBuffer[thread] = add64(accumBuffer[thread], to64(f));
    }
  }

  workgroupBarrier();

  if (thread > 0) { return; }

  var accumValue = 0.0;
  //var accumValue = vec2<u32>(0);
  for (var i = 0; i < 256; i++) {
    accumValue += accumBuffer[i];
    //accumValue = add64(accumValue, accumBuffer[i]);
  }

  let mean = accumValue;
  //let mean = from64(accumValue);
  meanBuffer[0] = select(0.0, f32(count) / mean, mean != 0.0);
}
