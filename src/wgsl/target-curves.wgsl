const PI = 3.141592653589793;

@export fn getLinear(t: f32, r: vec2<f32>) -> f32 {
  let a = r.x;
  let b = r.y;
  return clamp((t - a) / (b - a), 0.0, 1.0);
}

@export fn getCosine(t: f32, r: vec2<f32>) -> f32 {
  let f = getLinear(t, r);
  let q = .5 - .5 * cos(f * PI);
  return q;
}

@export fn getCosineQ(t: f32, r: vec2<f32>) -> f32 {
  let f = getLinear(t, r);
  let q1 = .5 - .5 * cos(f * PI);
  let q2 = .5 - .5 * cos(q1 * PI);
  return q2;
}

@export fn getComplement(t: f32, r: vec2<f32>) -> f32 {
  let f = getLinear(t, r);
  let c = cos(f * PI / 2.0);
  let c2 = c*c;
  let c4 = c2*c2;
  let c8 = c4*c4;

  let s = clamp(1.0 - c8, 0.0, 1.0);
  let s2 = s*s;
  let s4 = s2*s2;
  let s8 = s4*s4;

  return s8;
};
