use './array'::{ sizeToModulus3, packIndex3 };

@link fn getSize() -> vec3<u32>;
@link fn getWindow(i: u32) -> f32;

@export fn targetStored(fx: f32, fy: f32, fz: f32) -> f32 {

  let size = getSize();
  let xyz = vec3<f32>(fx, fy, fz) * vec3<f32>(size) / 2.0;

  let modulus = sizeToModulus3(size);
  let index = packIndex3(vec3<u32>(xyz), modulus);

  return getWindow(index);
};
