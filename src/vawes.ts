export const foo = 1;

const gen = (k: number, n: number) => {
  const angles = seq(n).map(i => Math.round(i/n * k * 360));
  return angles.map(a => '<div style="padding: 0 4px; transform: rotate(' + a + 'deg)">Δ</div>').join('');
};

const gen2 = (k1: number, k2: number, n: number) => {
  const angles = seq(n).map(i => {
    const angle1 = (i/n * k1 * π * 2);
    const angle2 = (i/n * k2 * π * 2);

    const x = Math.cos(angle1) - Math.cos(angle2);
    const y = Math.sin(angle1) - Math.sin(angle2);

    const a = (Math.atan2(y, x) * 180 / π).toFixed(3);
    const r = Math.sqrt(x*x + y*y).toFixed(3);

    return [a, r];
  });

  return angles.map(([a, r]) => '<div style="padding: 0 4px; transform: scale(' + r + ', ' + r + ') rotate(' + a + 'deg)">Δ</div>').join('');
};

console.log(gen(2, 8))
console.log(gen(5, 8))

console.log(gen2(2, 5, 8))
