import React, { FC, useLayoutEffect, useRef, useState } from 'react';

export type TooltipProps = {
  label: string,
  x?: number,
  y?: number,
  children: any,
};

export const Tooltip: FC<TooltipProps> = (props: TooltipProps) => {
  const {x = 0, y = 0, label, children} = props;

  const [hover, setHover] = useState(false);

  const hoverOn = () => setHover(true);
  const hoverOff = () => setHover(false);

  const clickOn = () => {
    if (hover) return clickOff();

    hoverOn();
    document.addEventListener('onPointerDown', clickOff);
  };
  
  const clickOff = () => {  
    hoverOff();
    document.removeEventListener('onPointerDown', clickOff);
  };

  const ref = useRef<HTMLDivElement>(null);
  useLayoutEffect(() => {
  });

  return (<>
    <div onMouseEnter={hoverOn} onMouseLeave={hoverOff} onPointerDown={clickOn} style={{position: 'relative'}}>
      <Tip style={{opacity: hover ? 1 : 0, marginLeft: x, marginTop: y}}>{label}</Tip>
      {children}
    </div>
  </>);
};

export const Tip = (props) => (
  <div style={{
    ...props.style,
    position: 'absolute',
    background: '#338',
    color: '#fff',
    padding: '10px 20px',
    borderRadius: '4px',
    transform: 'translate(-100%, 0)',
    fontSize: '14px',
    fontWeight: 'normal',
    border: '1px solid #66b',
    boxShadow: '0 2px 5px rgba(0, 0, 0, .5)',
    zIndex: 100,
    whiteSpace: 'nowrap',
    pointerEvents: 'none',
    transition: 'opacity .15s ease-in-out',
  }}>
    {props.children}
  </div>
);

export const Help = (props) => <div style={{
  ...props.style,
  display: 'inline-block',
  width: 20,
  height: 20,
  position: 'relative',
  top: 1,
  border: '2px solid #888',
  borderRadius: '100%',
}}>
  <div style={{
    position: 'absolute',
    left: 7,
    top: 6,
    width: 2,
    height: 7,
    borderRadius: '2px',
    background: '#888',
  }} />
  <div style={{
    position: 'absolute',
    left: 7,
    top: 3,
    width: 2,
    height: 2,
    borderRadius: '0px',
    background: '#888',
  }} />
</div>;

