import React, { useState } from 'react';

const defaultFormatter = (x) => x.toString();
const defaultParser = (s) => parseFloat(s);

type NumberInputProps = {
  disabled?: boolean,
  width?: number,
  value?: number,
  placeholder?: string,
  onChange?: (n: number) => void,
  format?: (n: number) => string,
  parse?: (s: string) => number,
};

export const NumberInput: FC<NumberInputProps> = (props: NumberInputProps) => {
  const {
    width = 100,
    disabled,
    value,
    onChange,
    placeholder,
    format = defaultFormatter,
    parse = defaultParser,
  } = props;

  const [text, setText] = useState(() => format(value) ?? '');
  const [focused, setFocused] = useState(false);

  const [lastValue, setLastValue] = useState(value);
  if (lastValue !== value) {
    setLastValue(value);
    setText(format(value) ?? '');
  }

  const handleFocus = (e) => setFocused(true);
  const handleBlur = (e) => setFocused(false);

  const handleChange = (e) => {
    const text = e.target.value;
    setText(text);

    const value = parse(text);
    if (value !== undefined && !Number.isNaN(value)) {
      setLastValue(value);
      if (onChange) onChange(value);
    }
  };

  return (
    <input
      style={{width, padding: '2px 5px', textAlign: 'right', background: '#444', color: '#fff', border: 0, borderRadius: 2}}
      placeholder={placeholder}
      disabled={disabled}
      value={text}
      onBlur={handleBlur}
      onFocus={handleFocus}
      onChange={handleChange}
    />
  )
};
