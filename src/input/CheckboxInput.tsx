import React from 'react';

type CheckboxInputProps = {
  id?: string,
  disabled?: boolean,
  width?: number,

  value?: boolean,
  onChange?: (value: boolean) => void,
};

export const CheckboxInput: FC<CheckboxInputProps> = (props: CheckboxInputProps) => {
  const {
    id,
    disabled,
    options,
    value,
    onChange,
  } = props;

  return (
    <input id={id} type="checkbox" disabled={disabled} onChange={(e) => onChange(e.target.checked)} checked={value} />
  );
};
