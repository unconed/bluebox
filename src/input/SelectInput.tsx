import React from 'react';

type SelectOption<T> = { value: T, label: string };

type SelectInputProps<T extends string = string> = {
  disabled?: boolean,
  width?: number,

  options?: SelectOption<T>[],

  value?: T,
  onChange?: (value: T) => void,
};

export const SelectInput: FC<SelectInputProps<unknown>> = <T extends string>(props: SelectInputProps<T>) => {
  const {
    width,
    disabled,
    options,
    value,
    onChange,
  } = props;

  return (
    <select disabled={disabled} style={{width, fontSize: 14, padding: '2px 4px', background: "#444", color: '#fff'}} value={value} onChange={(e) => onChange(e.target.value)}>
      {options.map(({value, label}) => <option key={value} value={value}>{label}</option>)}
    </select>
  );
};
