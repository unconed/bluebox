import React, { useCallback, useMemo, useRef, useState, forwardRef } from 'react';
import { clamp } from '@use-gpu/core';

type ArrowFunction = (...args: any[]) => any;

type SliderInputProps<T extends (number | [number, number])> = {
  disabled?: boolean,
  width?: number,
  height?: number,

  min?: number,
  max?: number,
  step?: number,

  direction?: 'x' | 'y',
  value?: T,

  onChange?: (value: T) => void,
};

const TRACK = 4;
const THUMB = 16;

export const SliderInput: FC<SliderInputProps> = (props: SliderInputProps) => {
  const {
    disabled,
    options,
    value,
    min = 0,
    max = 100,
    step = 1,
    width,
    height,
    direction = 'x',
    onChange,
  } = props;

  /*
    <input 
      type="range"
      disabled={disabled}
      min={min}
      max={max}
      step={step}
      value={value}
      style={{width}}
      onChange={(e) => onChange(+e.target.value)}
    />
  */

  const isX = direction === 'x';
  const isRange = Array.isArray(value);
  
  const start = isRange ? value[0] : value;
  const end = isRange ? value[1] : vlaue;
  
  const f1 = clamp((start - min) / (max - min), 0, 1);
  const f2 = clamp((end - min) / (max - min), 0, 1);
  
  const w = width ?? (!isX ? 20 : 100);
  const h = height ?? (isX ? 20 : 100);

  const leftS = isX ? Math.round(f1 * (width - THUMB)) + THUMB / 2 : 0;
  const topS = !isX ? Math.round(f1 * (height - THUMB)) + THUMB / 2 : 0;

  const leftE = isX ? Math.round(f2 * (width - THUMB)) + THUMB / 2 : 0;
  const topE = !isX ? Math.round(f2 * (height - THUMB)) + THUMB / 2 : 0;

  const [dragging, setDragging] = useState(false);
  const [key, setKey] = useState('');
  const [offset, setOffset] = useState([0, 0]);

  const trackRef = useRef<HTMLDivElement>(null);

  const draggingRef = useRef(dragging);
  const valueRef = useRef(value);
  const keyRef = useRef(key);
  const offsetRef = useRef(offset);
  draggingRef.current = dragging;
  valueRef.current = value;
  keyRef.current = key;
  offsetRef.current = offset;

  const handleTrack = useCallback(() => {
  }, []);

  const pointerUpFixRef = useRef<ArrowFunction | null>(null);
  const handlePointerDown = useCallback((key: string | null) => (e: PointerEvent) => {
    (e.target as any).setPointerCapture(e.pointerId);
    
    const {current: track} = trackRef;
    if (!track) return;

    const { clientX, clientY, target } = e;
    const { left: l, top: t, width, height } = track.getBoundingClientRect();

    let resolved = key;
    let base = 0;
    if (resolved == null) {
      const pos = isX ? clientX - l : clientY - t;
      const size = isX ? width : height;
      const val = clamp(min + Math.round(pos / size * (max - min) / step) * step, min, max);
      console.log({start, end, val})
      if (val < start) resolved = 'start';
      else if (val > end) resolved = 'end';
      else {
        if (Math.abs(val - start) < Math.abs(val - end)) resolved = 'start';
        else resolved = 'end';
      }
      base = pos + THUMB/2;
      onChange?.(resolved === 'start' ? [Math.min(val, end - step), end] : [start, Math.max(start + step, val)]);
    }
    else {
      base = isX ? (resolved === 'start' ? leftS : leftE) : (resolved === 'start' ? topS : topE);
    }

    setDragging(true);
    setKey(resolved);
    setOffset(
      isX ? [
        clientX - base + THUMB/2,
        width,
      ] : [
        clientY - base + THUMB/2,
        height,
      ]
    );
    
    // Workaround for chrome bug - macOS pointerUp while moving mouse does not fire when pointer capture is active
    // https://bugs.chromium.org/p/chromium/issues/detail?id=1459632&q=pointerup%20event&can=2
    document.addEventListener('pointerup', handlePointerUp);
    pointerUpFixRef.current = handlePointerUp;

    e.preventDefault();
    e.stopPropagation();
  }, [isX, leftS, topS, leftE, topE, min, max, step]);

  const handlePointerMove = useCallback((e: PointerEvent) => {
    const { current: dragging } = draggingRef;
    const { current: key } = keyRef;
    const { current: [base, size] } = offsetRef;

    if (!dragging) return;
    
    const { clientX, clientY } = e;
    const pos = isX ? clientX - base : clientY - base;
    const val = clamp(min + Math.round(pos / size * (max - min) / step) * step, min, max);

    if (isRange) {
      const { current: [start, end] } = valueRef;
      onChange?.(key === 'start' ? [Math.min(val, end - step), end] : [start, Math.max(start + step, val)]);
    }
    else {
      onChange?.(val);
    }

    e.preventDefault();
    e.stopPropagation();
  }, [isX, isRange, min, max, step]);

  const handlePointerUp = useCallback((e: PointerEvent) => {
    (e.target as any).releasePointerCapture(e.pointerId);

    const {current: upHandler} = pointerUpFixRef;
    if (upHandler) {
      document.removeEventListener('pointerup', handlePointerUp);
      pointerUpFixRef.current = null;
    }

    setDragging(false);
    setKey(null);
    setOffset([0, 0]);

    e.preventDefault();
    e.stopPropagation();
  }, []);

  const handlersT = useMemo(() => ({
    onPointerDown: handlePointerDown(null),
    onPointerMove: handlePointerMove,
    onPointerUp: handlePointerUp,
  }), [handlePointerDown, handlePointerMove, handlePointerUp]);

  const handlersS = useMemo(() => ({
    onPointerDown: handlePointerDown('start'),
    onPointerMove: handlePointerMove,
    onPointerUp: handlePointerUp,
  }), [handlePointerDown, handlePointerMove, handlePointerUp]);

  const handlersE = useMemo(() => ({
    onPointerDown: handlePointerDown('end'),
    onPointerMove: handlePointerMove,
    onPointerUp: handlePointerUp,
  }), [handlePointerDown, handlePointerMove, handlePointerUp]);

  return (
    <Container
      style={{width, height}}
      handlers={disabled ? undefined : handlersT}
    >
      <Track
        ref={trackRef}
        direction={direction}
      />
      <Thumb
        disabled={disabled}
        dragging={key === 'start'}
        direction={direction}
        tabIndex={0}
        style={{left: leftS, top: topS}}
        handlers={disabled ? undefined : handlersS}
      />
      {end != null ? <Thumb
        disabled={disabled}
        dragging={key === 'end'}
        direction={direction}
        tabIndex={0}
        style={{left: leftE, top: topE}}
        handlers={disabled ? undefined : handlersE}
      /> : null}
    </Container>
  );
};

export const Container = (props) => <div style={{
  ...props.style,
  position: 'relative',
  height: 20,
}} {...props.handlers}>{props.children}</div>;

export const Track = forwardRef((props, ref) => <div ref={ref} style={{
  ...props.style,
  position: 'absolute',
  ...props.direction === 'x' ? {
    left: THUMB / 2,
    right: THUMB / 2,
    top: '50%',
    marginTop: -TRACK / 2,
    height: TRACK,
  } : {
    top: THUMB / 2,
    bottom: THUMB / 2,
    left: '50%',
    marginLeft: -TRACK / 2,    
    width: TRACK,
  },
  borderRadius: TRACK / 2,
  background: '#888',
}} {...props.handlers}>{props.children}</div>);

export const Thumb = (props) => <div style={{
  ...props.style,
  position: 'absolute',
  width: THUMB,
  height: THUMB,
  marginLeft: -THUMB / 2,
  marginTop: -THUMB / 2,
  background: props.dragging ? '#fff' : props.disabled ? '#888' : '#ccc',
  borderRadius: '100%',
  zIndex: props.dragging ? 2 : 1,
  ...props.direction === 'x' ? { top: '50%' } : { left: '50% '},
}} {...props.handlers}>{props.children}</div>;
