import { useResource } from '@use-gpu/live';
import { useForceUpdate } from '@use-gpu/workbench';

export const useResize = () => {
  const [version, forceUpdate] = useForceUpdate();
  useResource((dispose) => {
    window.addEventListener('resize', forceUpdate);
    dispose(() => window.removeEventListener('resize', forceUpdate));
  }, [forceUpdate]);
  
  const {width, height} = document.querySelector('canvas').getBoundingClientRect();
  return [width, height, forceUpdate];
};

