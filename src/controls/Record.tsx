import React, { Provide, Quote, Yeet, useContext, useMemo, useOne, useState } from '@use-gpu/live';
import { Loop, TimeContext, FrameContext, RenderContext } from '@use-gpu/workbench';
import { patch } from '@use-gpu/state';
import { RecordControls } from './RecordControls';
import saveAs from 'file-saver';

type RecordProps = {
  onRecord?: (x: boolean) => {},
};

export const Record = (props: RecordProps) => {
  const {onRecord} = props;
  
  const [recorder, setRecorder] = useState<any>(null);
  const [track, setTrack] = useState<any>(null);
  const [frame, setFrame] = useState(0);
  const [timer, setTimer] = useState(0);

  const [options, setOptions] = useState({
    width: 760,
    height: 428,
    dpi: 2,
    from: 0,
    to: 10,
    fps: 60,
  });
  const updateOptions = (u: any) => setOptions(s => patch(s, u));

  const {from, to, fps, dpi} = options;
  const delta = 1 / fps;
  const timestamp = from + frame * delta;

  const renderContext = useContext(RenderContext);
  const fixedContext = useMemo(() => ({
    ...renderContext,
    pixelRatio: dpi,
  }), [renderContext, dpi]);
  
  const record = () => {
    const {width, height, dpi, from, to, fps} = options;
    const container = document.querySelector('#gpu-box');
    const relativeDPI = dpi / window.devicePixelRatio;

    container.style.width = (width * relativeDPI) + 'px';
    container.style.height = (height * relativeDPI) + 'px';

    const canvas = document.querySelector('canvas');
    if (!canvas) return;

    setTimeout(() => {
      onRecord?.(true);

      const stream = canvas.captureStream(0);
      const recorder = new MediaRecorder(stream, {
        mimeType: 'video/x-matroska;codecs=avc1',
        videoBitsPerSecond: 4000000,
      });

      recorder.start();
      setRecorder(recorder);
  
      const [track] = stream.getVideoTracks();
      setTrack(track);
      setFrame(0);

      setTimer(setInterval(() => setFrame(f => f + 1), 1000 * delta));
    }, 16);
  };

  const stop = () => {
    if (!recorder) return;

    const container = document.querySelector('#gpu-box');
    container.style.width = '100%';
    container.style.height = '100%';

    recorder.addEventListener('dataavailable', (e: BlobEvent) => {
      if (e.data.size) saveAs(e.data, 'video.mkv');
    });
    recorder.requestData();
    recorder.stop();    
    onRecord?.(false);

    setRecorder(null);
    setTrack(null);
    setTimer(0);
    clearInterval(timer);
  };
  
  const render = () => {
    if (!track) return;

    track.requestFrame();

    if (timestamp >= to) stop();
  };

  return (<>
    {recorder && track ? (
      <Provide context={TimeContext} value={{
        timestamp: timestamp * 1000,
        elapsed: timestamp * 1000,
        delta: delta * 1000,
      }}>
        <Provide context={FrameContext} value={frame}>
          <Provide context={RenderContext} value={fixedContext}>
            {props.children}
            <Quote><Yeet>{render}</Yeet></Quote>
          </Provide>
        </Provide>
      </Provide>
    ) : (
      <Loop>
        {props.children}
      </Loop>
    )}
    <RecordControls
      frame={frame}
      timestamp={timestamp}
      recording={!!recorder}
      record={record}
      stop={stop}
      options={options}
      onOptionsChange={updateOptions}
    />
  </>);
};