import React, { LC } from '@use-gpu/live';
import { Block, Inline, Text } from '@use-gpu/layout';

const MARGIN_TOP = [0, 3, 0, 0];
const WHITE_TRANSPARENT = [1, 1, 1, 0.75];

type LabelProps = {
  size?: number,
  weight?: string | number,
};

export const Label: LC<LabelProps> = (props: PropsWithChildren<LabelProps>) => (
  <Block margin={MARGIN_TOP}>
    <Inline align={"center"}>
      <Text
        family={"Lato"}
        size={props.size ?? 10}
        weight={props.weight}
        snap={false}
        text={Array.isArray(props.children) ? props.children.join('') : props.children}
        color={WHITE_TRANSPARENT}
      />
    </Inline>
  </Block>
);
