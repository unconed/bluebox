import type { StorageTarget } from '@use-gpu/core';

import { uploadBuffer, seq } from '@use-gpu/core';

export const initRandom = (
  device: GPUDevice,
  target: StorageTarget,
) => {
  const {buffer, size, length} = target;

  const array = getUniformRandom(length);
  uploadBuffer(device, buffer, array.buffer);
  return checkHistogram(array, size, true);
};

const getUniformRandom = (length: number) => {
  const order = seq(length);
  const r = order.map(Math.random);
  order.sort((a, b) => r[a] - r[b]);

  const equal = (i: number) => {
    const f = 256 / Math.min(256, length);
    const j = order[i];
    const x = (j * f + Math.floor(f / 2)) % 256;
    return x / 255 * 2 - 1;
  };

  const array = new Float32Array(length);
  for (let i = 0; i < length; ++i) {
    array[i] = equal(i);
  }

  return array;
}

export const checkHistogram = (vs: TypedArray, size: number[], verbose: boolean = false) => {
  const [w, h, d] = size;
  const wh = w * h;
  const whd = wh * d;
  
  const histo = seq(256).map(_ => 0);

  let o = 0;
  for (let i = 0; i < whd; ++i) {
    histo[Math.round((vs[o] * .5 + .5) * 255)]++;
    o++;
  }
  
  const counts: any = {};
  for (let i = 0; i < 256; ++i) if (histo[i]) counts[histo[i]] = (counts[histo[i]]||0) + 1;

  const valid = Object.keys(counts).length === 1;
  if (!valid) {
    if (verbose) {
      console.warn("❌ Histogram check failed!");
      console.warn(histo);
      console.warn(counts);
    }
    return false;
  }
  else {
    return true;
  }
}
