import type { StorageSource } from '@use-gpu/core';
import type { ShaderSource } from '@use-gpu/shader';

export type GenCurve = 'linear' | 'cosine' | 'cosine2' | 'cosineq' | 'rolloff' | 'rolloffq' | 'rolloffq2';

export type GenDisplay = {
  gamma: boolean,
  zSlices: boolean,
  wordcel: boolean,
};

export type GenStats = {
  score: number,
  rate: number,
  success: number,
};

export type GenState = {
  width: number,
  height: number,
  depth: number,
  
  space: GenCurve,
  time: GenCurve,
  temporal: boolean,

  spaceRange: [number, number],
  timeRange: [number, number],

  gradient: boolean,
  sparse: boolean,
  autoBatch: boolean,

  batchSize: number,
  blockSize: number,
};

export type TargetFn = {
  shader: ShaderSource,
  norm: number,
  curves: Record<string, number[]>,
};
