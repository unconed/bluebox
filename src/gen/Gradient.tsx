import type { LC } from '@use-gpu/live';
import type { StorageSource, StorageTarget } from '@use-gpu/live';
import type { ShaderSource } from '@use-gpu/shader/wgsl';
import type { TargetFn } from './types';

import { memo, yeet, use, wrap, useMemo, useOne, useVersion } from '@use-gpu/live';
import { Compute, Dispatch, Readback, useBoundShader, useDeviceContext, useScratchSource, useShaderRef } from '@use-gpu/workbench';
import { clearBuffer, uploadBuffer } from '@use-gpu/core';

import { swapMean } from '../wgsl/swap-mean.wgsl';
import { swapGradient } from '../wgsl/swap-gradient.wgsl';

import { FFT } from './FFT';
import { iir } from '../util';

export type GradientProps = {
  timeBuffer: StorageTarget,
  freqBuffer: StorageTarget,
  gradientBuffer: StorageTarget,

  batch: number,
  targetFn: TargetFn,
  version: number,
};

const isPowerOf2 = (x: number) => !(x & (x - 1));

export const Gradient: LC<GradientProps> = memo((props: GradientProps) => {
  const {
    timeBuffer,
    freqBuffer,
    gradBuffer,

    batch = 1,
    targetFn,
    version,
  } = props;

  const {size, length} = freqBuffer;
  const [w, h, d] = size;

  const device = useDeviceContext();

  const s = useShaderRef(size);

  const options = useOne(() => ({
    flags: GPUBufferUsage.STORAGE | GPUBufferUsage.COPY_SRC | GPUBufferUsage.COPY_DST,
    readWrite: true,
    reserve: 16,
  }));
  const [mean] = useScratchSource('f32', options);

  const meanShader = useBoundShader(swapMean, [s, freqBuffer, mean]);
  const gradientShader = useBoundShader(swapGradient, [s, targetFn.shader, freqBuffer, mean]);

  const targetVersion = useVersion(targetFn);

  let iter = 0;

  let lastTime = 0;
  let lastBest = 0;
  
  const rateSmooth = iir(0.125);

  const shouldDispatch = () => (iter % batch) === 0;

  const dispatch = useMemo(() => [
    yeet({ compute: () => {
      shouldDispatch() && clearBuffer(device, mean.buffer)
    }}),
    use(FFT, {
      source: {...timeBuffer},
      target: freqBuffer,
      shouldDispatch,
    }),
    use(Dispatch, {
      size: [64, 1, 1],
      group: [64, 1, 1],
      shader: meanShader,
      shouldDispatch,
    }),
    use(Dispatch, {
      size,
      group: [4, 4, 4],
      shader: gradientShader,
      shouldDispatch,
    }),
    use(FFT, {
      source: {...freqBuffer},
      target: gradBuffer,
      inverse: true,
      shouldDispatch,
    }),
    /*
    use(Readback, {
      source: gradBuffer,
      shouldDispatch,
      then: (xs: number[]) => {
        console.log(xs.slice(0, 16));
      },
    }),
    */
    yeet({ compute: () => iter++ }),
  ], [size, batch, timeBuffer, freqBuffer, gradBuffer, meanShader, gradientShader, targetVersion]);

  return dispatch;
}, 'Gradient');
