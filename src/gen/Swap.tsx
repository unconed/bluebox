import type { LC } from '@use-gpu/live';
import type { StorageSource, StorageTarget } from '@use-gpu/live';
import type { ShaderSource } from '@use-gpu/shader/wgsl';
import type { TargetFn } from './types';

import { memo, yeet, use, useMemo, useOne, useVersion } from '@use-gpu/live';
import { Dispatch, Readback, useBoundShader, useDeviceContext, useScratchSource, useShaderRef } from '@use-gpu/workbench';
import { clearBuffer, uploadBuffer } from '@use-gpu/core';

import { swapMean } from '../wgsl/swap-mean.wgsl';
import { swapScore } from '../wgsl/swap-score.wgsl';
import { swapTarget } from '../wgsl/swap-target.wgsl';

import { swapApply } from '../wgsl/swap-apply.wgsl';

import { FFT } from './FFT';
import { iir, len2 } from '../util';

export type SwapProps = {
  timeBuffer: StorageTarget,
  freqBuffer: StorageTarget,
  gradBuffer: StorageTarget,

  targetFn: TargetFn,
  statsBuffer: StorageTarget,

  batch: number,
  version: number,
  gradient?: boolean,
};

const isPowerOf2 = (x: number) => !(x & (x - 1));

export const Swap: LC<SwapProps> = memo((props: SwapProps) => {
  const {
    timeBuffer,
    freqBuffer,
    gradBuffer,

    targetFn,
    statsBuffer,

    batch = 100,
    version,
    gradient,
  } = props;

  const {size, length} = freqBuffer;
  const [w, h, d] = size;

  const device = useDeviceContext();

  const l = useShaderRef(w * h * d);
  const s = useShaderRef(size);
  const b = useShaderRef(batch);

  const options = useOne(() => ({
    flags: GPUBufferUsage.STORAGE | GPUBufferUsage.COPY_SRC | GPUBufferUsage.COPY_DST,
    readWrite: true,
    reserve: 16,
  }));
  const [mean] = useScratchSource('f32', options);
  const [score] = useScratchSource('f32', options);
  const [swap] = useScratchSource('u32', options);
  const [_dummy] = useScratchSource('f32', options);

  const g = useShaderRef(gradient);
  const i = useShaderRef(0);
  const s1 = useShaderRef(0);
  const s2 = useShaderRef(0);

  const defs = useOne(() => ({
    SCORE_ONLY: false,
    SCORE_COMPARE: true,
    USE_GRADIENT: gradient,
    SWAP_SPARSE: false,
    SWAP_GENERATE: true,
    SWAP_APPLY: true,
    SWAP_WARMUP: true,
  }), gradient);
  const defs1 = useOne(() => ({...defs, SWAP_UNDO: false}), defs);
  const defs2 = useOne(() => ({...defs, SWAP_UNDO: true}), defs);

  const swapMaybeShader = useBoundShader(swapApply, [timeBuffer, gradBuffer, statsBuffer, b, i, l, s1, s2, score, swap], defs1);
  const swapUndoShader = useBoundShader(swapApply, [timeBuffer, gradBuffer, statsBuffer, b, i, l, s1, s2, score, swap], defs2);

  const meanShader = useBoundShader(swapMean, [s, freqBuffer, mean]);
  const scoreShader = useBoundShader(swapScore, [s, 0, targetFn.shader, freqBuffer, _dummy, mean, score], defs);

  const targetVersion = version + useVersion(targetFn);

  let scoreVersion = -1;
  let iter = 0;

  const dispatch = useMemo(() => [
    yeet({
      compute: () => {
        clearBuffer(device, mean.buffer);
        if (scoreVersion !== targetVersion) {
          clearBuffer(device, score.buffer);
          scoreVersion = targetVersion;
        }

        g.current = gradient;
        i.current  = iter;
        s1.current = Math.random();
        s2.current = Math.random();
        iter++;
      },
    }),
    use(Dispatch, {
      size: [1, 1, 1],
      group: [1, 1, 1],
      shader: swapMaybeShader,
    }),
    use(FFT, {
      source: timeBuffer,
      target: freqBuffer,
    }),
    use(Dispatch, {
      size: [256, 1, 1],
      group: [256, 1, 1],
      shader: meanShader,
    }),
    use(Dispatch, {
      size: [256, 1, 1],
      group: [256, 1, 1],
      shader: scoreShader,
    }),
    use(Dispatch, {
      size: [1, 1, 1],
      group: [1, 1, 1],
      shader: swapUndoShader,
    }),
    /*
    use(Readback, {
      source: swap,
      then: (swaps: number[]) => console.log('swap', swaps[0], swaps[1])
    }),
    use(Readback, {
      source: score,
      then: (scores: number[]) => {
        let last = scores[1];
        let current = scores[0];
        if (current < last) c++;
        t++;
        console.log('score', last, current, +(current < last));
    }}),
    */
  ], [device, size, mean, timeBuffer, freqBuffer, meanShader, scoreShader, swapMaybeShader, swapUndoShader, targetVersion, gradient]);

  return dispatch;
}, 'Swap');
