import type { LC } from '@use-gpu/live';
import { yeet, useMemo } from '@use-gpu/live';

import { useBoundShader, useDerivedSource, useRawSource, useShaderRef } from '@use-gpu/workbench';
import { clamp, seq } from '@use-gpu/core';

import { CURVES_JS, getTargetJS, getTargetWGSL } from './curves';
import { fft3d } from './fft3d';
import { len2, len3 } from '../util';

import { targetStored } from '../wgsl/target-stored.wgsl';

const π = Math.PI;
const τ = π * 2;

export type WindowedTargetProps = {
  width: number,
  height: number,
  depth: number,
  space: string,
  time: string,
  temporal: boolean,
};

const USE_WINDOW = false;

export const WindowedTarget: LC<WindowedTargetProps> = (props: WindowedTargetProps) => {
  const {width, height, depth, space, time, temporal, spaceRange, timeRange} = props;

  const ts = useShaderRef(spaceRange);
  const tt = useShaderRef(timeRange);

  const [norm, curves, size, length, buffer] = useMemo(() => {
    const getSpace = CURVES_JS[space];
    const getTime = CURVES_JS[time];
    
    const getTarget = getTargetJS(space, time, temporal, spaceRange, timeRange, depth);

    const getWindow = (t: number) => 1.0 - (3 - 2*t)*t*t;

    const curves = {};

    const ix = 2/width;
    const iy = 2/height;
    const iz = 2/depth;

    const size = [width, height, depth];
    const length = width * height * depth;
    const w = width;
    const wh = width * height;

    const ws = new Float32Array(length * 2);
    const vs = new Float32Array(length * 2);
    const fs = new Float32Array(length * 2);

    if (USE_WINDOW) {
      // Time domain window
      for (let i2 = 0, z = 0; z < depth; z++) {
        const dz = z * iz;
        const fz = depth > 1 ? dz - 1 : 0;
        for (let y = 0; y < height; y++) {
          const dy = y * iy;
          const fy = height > 1 ? dy - 1 : 0;
          for (let x = 0; x < width; x++) {
            const dx = x * ix;
            const fx = width > 1 ? dx - 1 : 0;
            const dr = Math.min(1, len3(fx, fy, fz));

            const w = getWindow(dr);
            ws[i2]   = w;
            ws[i2+1] = 0;

            i2 += 2;
          }
        }
      }

      // Remove phase component
      fft3d(ws, fs, size);

      for (let i = 0; i < length; i++) {
        let i2 = i * 2;
        fs[i2] = len2(fs[i2], fs[i2+1]);
        fs[i2+1] = 0;
      }

      fft3d(fs, ws, size, {inverse: true});
    }

    // Freq domain target
    for (let i2 = 0, z = 0; z < depth; z++) {
      const dz = z * iz;
      const fz = Math.min(dz, 2 - dz);
      for (let y = 0; y < height; y++) {
        const dy = y * iy;
        const fy = Math.min(dy, 2 - dy);
        for (let x = 0; x < width; x++) {
          const dx = x * ix;
          const fx = Math.min(dx, 2 - dx);
          const r = getTarget(fx, fy, fz);
          fs[i2] = r;
          fs[i2+1] = 0;
          i2 += 2;
        }
      }
    }

    if (USE_WINDOW) {
      // Convolve in freq domain = multiply in time domain
      fft3d(fs, vs, size, {inverse: true});

      for (let i = 0; i < length; i++) {
        let i2 = i * 2;
        vs[i2] *= ws[i2];
        vs[i2+1] *= ws[i2+1];
      }

      fft3d(vs, fs, size);
    }

    // Normalize magnitude distribution
    let accum = 0;
    for (let i = 0; i < length; i++) {
      let i2 = i * 2;
      const l = len2(fs[i2], fs[i2+1]);
      fs[i2] = l;
      fs[i2+1] = 0;
      accum += l;
    }

    const norm = width * height * depth / accum;

    for (let i = 0; i < length; i++) {
      let i2 = i * 2;
      fs[i2] *= norm;
    }

    return [norm, curves, size, length, fs];
  }, [width, height, depth, space, time, temporal, spaceRange, timeRange]);

  const data = useRawSource(buffer, 'vec2<f32>');

  const bound = useBoundShader(targetStored, [size, data]);
  const shader = useDerivedSource({shader: bound}, {size: () => size, length: () => length});
  const source = useDerivedSource({...data}, {size: () => size});

  return useMemo(() => yeet({shader, source, norm: 1, curves}), [shader, source, norm, curves]);
};
