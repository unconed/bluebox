import { seq, clamp } from '@use-gpu/core';
import { useBoundShader, useShaderRef } from '@use-gpu/workbench';
import { bindBundle } from '@use-gpu/shader/wgsl';

import { 
  getLinear as getLinearWGSL,
  getCosine as getCosineWGSL,
  getCosineQ as getCosineQWGSL,
  getComplement as getComplementWGSL,
} from '../wgsl/target-curves.wgsl';

import { getTargetS2 } from '../wgsl/target-s2.wgsl';
import { getTargetS3 } from '../wgsl/target-s3.wgsl';
import { getTargetST } from '../wgsl/target-st.wgsl';

const π = Math.PI;

const sqr = (x: number) => x * x;
const cos = (x: number) => Math.cos(x);
const abs = (x: number) => Math.abs(x);
const select = (a: number, b: number, c: number) => c ? b : a;

export const len2 = (x: number, y: number) => Math.sqrt(sqr(x) + sqr(y));
export const len3 = (x: number, y: number, z: number) => Math.sqrt(sqr(x) + sqr(y) + sqr(z));

const getLinear = (t: number, r: [number, number]): number => {
  const [a, b] = r;
  return clamp((t - a) / (b - a), 0, 1);
}

const getCosine = (t: number, r: [number, number]): number => {
  let f = getLinear(t, r);
  let q = .5 - .5 * cos(f * π);
  return q;
}

const getCosineQ = (t: number, r: [number, number]): number => {
  let f = getLinear(t, r);
  let q1 = .5 - .5 * cos(f * π);
  let q2 = .5 - .5 * cos(q1 * π);
  return q2;
}

const getComplement = (t: number, r: [number, number]): number => {
  let f = getLinear(t, r);
  let c = cos(f * π / 2);
  let c2 = c*c;
  let c4 = c2*c2;
  let c8 = c4*c4;

  let s = clamp(1 - c8, 0, 1);
  let s2 = s*s;
  let s4 = s2*s2;
  let s8 = s4*s4;

  return s8;
};

export const CURVES_WGSL = {
  linear: getLinearWGSL,
  cosine: getCosineWGSL,
  cosineQ: getCosineQWGSL,
  complement: getComplementWGSL,
};

export const CURVES_JS = {
  linear: getLinear,
  cosine: getCosine,
  cosineQ: getCosineQ,
  complement: getComplement,
};

export const getTargetJS = (space: string, time: string, temporal: boolean, spaceRange: number[], timeRange: number[], depth: number) => {
  const getSpace = CURVES_JS[space];
  const getTime = CURVES_JS[time];
  
  const getTargetST = (fx: number, fy: number, fz: number) => getSpace(len2(fx, fy), spaceRange) * getTime(fz, timeRange);
  const getTargetS2 = (fx: number, fy: number, fz: number) => getSpace(len2(fx, fy), spaceRange);
  const getTargetS3 = (fx: number, fy: number, fz: number) => getSpace(len3(fx, fy, fz), spaceRange);

  const getTarget =
    depth > 1
      ? (temporal ? getTargetST : getTargetS3)
      : getTargetS2;

  return getTarget;
};

export const getTargetWGSL = (space: string, time: string, temporal: boolean, spaceRange: number[], timeRange: number[], depth: number, norm: number) => {
  const n = useShaderRef(norm);
  const ts = useShaderRef(spaceRange);
  const tt = useShaderRef(timeRange);

  return useBoundShader(
    depth > 1
      ? (temporal ? getTargetST : getTargetS3)
      : getTargetS2,
    [
      n,
      ts, CURVES_WGSL[space],
      tt, CURVES_WGSL[time],
    ],
  );
}
