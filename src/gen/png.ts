import type { StorageSource, StorageTarget } from '@use-gpu/core';
import { saveAs } from 'file-saver';
import { getDataArrayByteLength, uploadBuffer } from '@use-gpu/core';
import { checkHistogram } from './stage';

export const loadStorageFromPNG = (device: GPUDevice, target: StorageTarget, image: Image, size: number[]) => {
  const [w, h, d] = size;
  const hd = h * d;
  const whd = w * hd;

  const canvas = document.createElement('canvas');

  canvas.width = w;
  canvas.height = hd;

  const context = canvas.getContext('2d');
  context.drawImage(image, 0, 0);

  const {data} = context.getImageData(0, 0, w, hd);

  const values = new Float32Array(whd);
  for (let i = 0; i < whd; ++i) {
    values[i] = (data[i * 4] / 255) * 2 - 1;
  }

  uploadBuffer(device, target.buffer, values.buffer);
  return checkHistogram(values, size, true);
};

export const saveStorageToPNG = async (device: GPUDevice, source: StorageSource, target: StorageTarget, name: string) => {
  const {format, size, length} = source;
  const [w, h, d] = size;

  const byteLength = getDataArrayByteLength(format, length);

  const commandEncoder = device.createCommandEncoder();
  commandEncoder.copyBufferToBuffer(source.buffer, 0, target.buffer, 0, byteLength);
  const command = commandEncoder.finish();

  device.queue.submit([command]);
  await device.queue.onSubmittedWorkDone();
  await target.buffer.mapAsync(GPUMapMode.READ);

  const array = new Float32Array(target.buffer.getMappedRange());

  const width = w;
  const height = h * d;

  const canvas = document.createElement('canvas');
  canvas.width = width;
  canvas.height = height;

  const context = canvas.getContext('2d');
	const imageData = context.createImageData(width, height);

  const n = length * 4;
  const rgba = imageData.data;
  for (let i = 0; i < length; i++) {
    const v = Math.round((array[i] * .5 + .5) * 255);

    const i4 = i*4;
    rgba[i4    ] = v;
    rgba[i4 + 1] = v;
    rgba[i4 + 2] = v;
    rgba[i4 + 3] = 255;
  }

  context.putImageData(imageData, 0, 0);

  target.buffer.unmap();

  const png = canvas.toBlob((blob: Blob) => {
    saveAs(blob, name);
  });
};
