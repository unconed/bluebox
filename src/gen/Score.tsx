import type { LC } from '@use-gpu/live';
import type { Lazy, StorageSource, StorageTarget } from '@use-gpu/core';
import type { ShaderSource } from '@use-gpu/shader/wgsl';
import type { TargetFn } from './types';

import { memo, yeet, use, useMemo, useOne, useVersion } from '@use-gpu/live';
import { Dispatch, Readback, useBoundShader, useDeviceContext, useScratchSource, useShaderRef } from '@use-gpu/workbench';
import { resolve, clearBuffer } from '@use-gpu/core';

import { swapMean } from '../wgsl/swap-mean.wgsl';
import { swapScore } from '../wgsl/swap-score.wgsl';
import { swapTarget } from '../wgsl/swap-target.wgsl';

import { swapApply } from '../wgsl/swap-apply.wgsl';

import { FFT } from './FFT';
import { iir, len2 } from '../util';

export type ScoreProps = {
  timeBuffer: StorageTarget,
  freqBuffer: StorageTarget,
  errorBuffer: StorageTarget,

  targetFn: TargetFn,
  statsBuffer: StorageTarget,
  freqTarget: StorageTarget,

  tickRef: Lazy<number>,
  batch: number,
  version: number,

  onScore: (score: number, rate: number) => void,
  onCount: (success: number, total: number) => void,
};

const isPowerOf2 = (x: number) => !(x & (x - 1));

export const Score: LC<ScoreProps> = memo((props: ScoreProps) => {
  const {
    timeBuffer,
    freqBuffer,
    errorBuffer,

    freqTarget,
    statsBuffer,
    targetFn,

    tickRef,
    batch,
    version,
    
    onScore,
    onCount,
  } = props;

  const {size, length} = freqBuffer;
  const [w, h, d] = size;

  const device = useDeviceContext();

  const s = useShaderRef(size);

  const options = useOne(() => ({
    flags: GPUBufferUsage.STORAGE | GPUBufferUsage.COPY_SRC | GPUBufferUsage.COPY_DST,
    readWrite: true,
    reserve: 16,
  }));
  const [mean] = useScratchSource('f32', options);
  const [score] = useScratchSource('f32', options);
  const [dummy] = useScratchSource('u32', options);

  const defs = useOne(() => ({
    SCORE_ONLY: true,
    SCORE_COMPARE: false,
  }));
  const meanShader = useBoundShader(swapMean, [s, freqBuffer, mean]);
  const scoreShader = useBoundShader(swapScore, [s, 0, targetFn.shader, freqBuffer, errorBuffer, mean, score], defs);
  const targetShader = useBoundShader(swapTarget, [s, targetFn.shader, freqTarget]);

  const targetVersion = version + useVersion(targetFn);

  let lastIter = 0;
  let lastTime = 0;
  let lastBest = 0;

  const percentSmooth = useOne(() => iir(0.05), targetVersion);
  const rateSmooth = useOne(() => iir(0.05), version + targetVersion);
  const successSmooth = useOne(() => iir(0.5), version + targetVersion);
  const speedSmooth = useOne(() => iir(0.1), targetVersion);

  const shouldDispatch = () => resolve(tickRef);

  const dispatch = useMemo(() => [
    yeet({ compute: () => {
      clearBuffer(device, mean.buffer);
      clearBuffer(device, score.buffer);
    }}),
    use(Dispatch, {
      size,
      group: [4, 4, 4],
      shader: targetShader,
      shouldDispatch: () => targetVersion,
    }),
    use(FFT, {
      source: timeBuffer,
      target: freqBuffer,
      shouldDispatch,
    }),
    use(Dispatch, {
      size: [256, 1, 1],
      group: [256, 1, 1],
      shader: meanShader,
      shouldDispatch,
    }),
    use(Dispatch, {
      size: [256, 1, 1],
      group: [256, 1, 1],
      shader: scoreShader,
      shouldDispatch,
    }),
    use(Readback, {
      source: statsBuffer,
      shouldDispatch,
      then: (counts: number[]) => {
        let [,,success] = counts;
        onCount?.(successSmooth(success), batch, percentSmooth(success / (batch||1) * 100));
      },
    }),
    use(Readback, {
      source: score,
      shouldDispatch,
      then: (scores: number[]) => {
        let [best] = scores;
        let rate = 0;
        let now = +new Date();

        let dt = (now - lastTime) / 1000;
        if (dt < 10/1000) return;

        const iter = resolve(tickRef);
        const reset = iter === 0 || iter < lastIter;

        if (reset) {
          lastBest = 0;
          lastTime = 0;

          onScore?.(best, 0, 0, 0);
        }
        else {
          if (lastTime) rate = (lastBest - best) / dt;
          const speed = iter > 1 ? (iter - lastIter) / dt : 0;
          onScore?.(best, rateSmooth(rate), iter, speed >= 0 ? speedSmooth(speed) : 0);

          lastBest = best;
          lastTime = now;
        }
        lastIter = iter;
      },
    }),
  ], [device, version, size, mean, batch, timeBuffer, freqBuffer, meanShader, scoreShader, targetShader, targetVersion, tickRef, onScore, onCount]);

  return dispatch;
}, 'Score');
