import React, { Reconcile, Gather, Fragment, Signal, LC, memo, useCallback, useContext, useMemo, useOne, useRef, useResource, useState } from '@use-gpu/live';
import type { StorageTarget } from '@use-gpu/core';
import type { TargetFn } from './types';

import { seq } from '@use-gpu/core';
import {
  LinearRGB, Pass, Flat, DataShader, ComputeBuffer, ComputeLoop, Compute, Suspense, PanControls, KeyboardContext, Loop, Cursor,
  useDeviceContext, useFontContext, DebugProvider, useScratchSource,
} from '@use-gpu/workbench';
import {
  UI, Layout, Absolute, Block, Flex, Overflow, Inline, Text, Embed,
} from '@use-gpu/layout';
import {
  Embedded, Cartesian, Grid, Axis, Sampled, Line, Surface,
} from '@use-gpu/plot';

import { useUpdateState } from '@use-gpu/state/live';
import { ShaderSource, wgsl } from '@use-gpu/shader/wgsl';
import { makeUseLocalState } from '../hooks/useLocalState';
import { useResize } from '../hooks/useResize';

import { Score } from './Score';
import { Swap } from './Swap';
import { SwapSparse } from './SwapSparse';
import { Gradient } from './Gradient';
import { Target } from './Target';
import { WindowedTarget } from './WindowedTarget';

import { Zoom } from '../html/Zoom';
import { GenControls, BATCH_SIZES } from './GenControls';
import { GenStatsPanel } from './GenStatsPanel';

import { initRandom } from './stage';
import { loadStorageFromPNG, saveStorageToPNG } from './png';

import { visualizeValue } from '../wgsl/viz-value.wgsl';
import { visualizeMagnitude } from '../wgsl/viz-magnitude.wgsl';
import { visualizePhase } from '../wgsl/viz-phase.wgsl';
import { visualizeAA } from '../wgsl/viz-aa.wgsl';

import { π, fmt } from '../util';

const DEFAULT_OPTIONS = {
  width: 64,
  height: 64,
  depth: 16,
  space: 'cosineQ',
  time: 'complement',
  temporal: true,
  gradient: true,
  sparse: false,
  autoBatch: true,
  spaceRange: [0.4, 1],
  timeRange: [0.25, 1],
  batchSize: 100,
  blockSize: 8,
};

const DEFAULT_DISPLAY = {
  gamma: true,
  zSlices: true,
  wordcel: true,
};

const GRAY = [0.5, 0.5, 0.5, 1.0];
const WHITE = [1,1,1,1];

const DEFAULT_STATS = {
  score: 0,
  rate: 0,
  success: 0,
  total: 0,
  percent: 0,
  steps: 0,
  speed: 0,
};

const BATCH_SIZE = 65536 * 100;
const BATCH_AUTO = 10;
const BATCH_FPS  = 1000/60;

const updateOptions = (u: any) => setOptions(s => patch(s, u));

export const Gen: LC = () => {

  const device = useDeviceContext();
  const [viewWidth, viewHeight] = useResize();

  const [options, updateOptions] = useUpdateState(DEFAULT_OPTIONS, makeUseLocalState('options'));
  const [display, updateDisplay] = useUpdateState(DEFAULT_DISPLAY, makeUseLocalState('display'));
  const [stats, updateStats] = useUpdateState(DEFAULT_STATS);

  const [valid, setValid] = useState(true);
  const [tick, setTick] = useState(0);
  const [inited, setInited] = useState(0);
  const [running, setRunning] = useState(false);

  const {width, height, depth, space, time, temporal, spaceRange, timeRange, gradient, sparse, autoBatch, blockSize, batchSize} = options;
  const {gamma, zSlices, wordcel} = display;
  const {score} = stats;

  const hasZ = depth > 1;

  const tile = viewWidth * 0.4;
  const boxScale = tile;
  const tileScale = hasZ && zSlices ? boxScale : Math.min(width * 4, boxScale);

  const handleGenerate = useCallback((target: StorageTarget) => {
    setValid(initRandom(device, target));
    setInited(i => i + 1);
    setRunning(true);
  }, [device]);

  const handleResize = useCallback((w: number, h: number, d: number, sparse: boolean) => {
    if (w !== width || h !== height || d !== depth) {
      setInited(i => 0);
      setRunning(false);
    }

    if (autoBatch) {
      const size = Math.min(500, Math.round(BATCH_SIZE / (width * height * depth) * (sparse ? 20 : 1)));
      const match = BATCH_SIZES.map((b, i) => [b, Math.min(size / b, b / size)]);
      match.sort((a, b) => b[1] - a[1]);

      updateOptions({ batchSize: match[0][0] });
    }
  }, [device, width, height, depth]);

  const handleStartStop = useCallback((target: StorageTarget) => {
    setRunning(r => !r);
  }, [device]);

  const isDefaults = JSON.stringify(DEFAULT_OPTIONS) === JSON.stringify(options);
  const handleDefaults = useCallback((target: StorageTarget) => {
    updateOptions(DEFAULT_OPTIONS);

    const {width: w, height: h, depth: d} = DEFAULT_OPTIONS;
    if (w !== width || h !== height || d !== depth) {
      setInited(0);
      setRunning(false);
    }
  }, [width, height, depth]);

  const readbackOptions = useOne(() => ({ flags: GPUBufferUsage.COPY_DST | GPUBufferUsage.MAP_READ }));
  const [saveBuffer, allocateSave] = useScratchSource('f32', readbackOptions);
  allocateSave(width * height * depth);

  const scoreRef = useRef(score);
  scoreRef.current = score;

  const handleLoad = useCallback((target: StorageTarget, blob: Blob) => {
    const size = [width, height, depth];
    setInited(i => i + 1);
    setTick(i => i + 1);

    const valid = loadStorageFromPNG(device, target, blob, size);
    setValid(valid);
  }, [device, width, height, depth]);

  const handleSave = useCallback((target: StorageTarget) => {
    const type = hasZ && temporal ? `${space}-${time}` : `${space}`;
    const size = `${width}x${height}x${depth}`;
    const quality = fmt(scoreRef.current, 8);
    const name = `noise-${size}-${type}-${quality}.png`;
    saveStorageToPNG(device, target, saveBuffer, name);
  }, [device, saveBuffer, width, height, depth, space, time, temporal]);

  const handleScore = useCallback((score, rate, steps, speed) => {
    requestAnimationFrame(() => updateStats({score, rate, steps, speed}));
  }, [updateStats]);

  const handleCount = useCallback((success, total, percent) => {
    requestAnimationFrame(() => updateStats({success, total, percent}));
  }, [updateStats]);

  const Wrapper = gamma ? LinearRGB : Fragment;
  const Swapper = sparse ? SwapSparse : Swap;

  const now = +new Date();
  const autoRef = useRef(0);
  const timeRef = useRef(now);
  const tickRef = useRef(tick);
  tickRef.current = tick;
  timeRef.current = now;
  useMemo(() => autoRef.current = 0, [viewWidth, viewHeight, options, display]);

  const runningRef = useRef(running);
  runningRef.current = running;

  const blockWidth = Math.min(width, blockSize);
  const blockHeight = Math.min(height, blockSize);
  const blockDepth = Math.min(depth, blockSize);

  const buffers = useMemo(() => [
    <ComputeBuffer
      width={width}
      height={height}
      depth={depth}
      format={'f32'}
      label="timeBuffer"
    />,
    <ComputeBuffer
      width={width}
      height={height}
      depth={depth}
      format={'vec2<f32>'}
      label="freqBuffer"
    />,
    <ComputeBuffer
      width={width}
      height={height}
      depth={depth}
      format={'vec2<f32>'}
      label="gradBuffer"
    />,

    <ComputeBuffer
      width={width}
      height={height}
      depth={depth}
      format={'vec2<f32>'}
      label="freqTarget"
    />,
    <ComputeBuffer
      width={width}
      height={height}
      depth={depth}
      format={'vec2<f32>'}
      label="freqBest"
    />,

    <ComputeBuffer
      width={width}
      height={height}
      depth={depth}
      format={'f32'}
      label="errorBuffer"
    />,
    <ComputeBuffer
      width={16}
      height={1}
      depth={1}
      format={'u32'}
      label="statsBuffer"
    />,

    <ComputeBuffer
      width={blockWidth}
      height={blockHeight}
      depth={blockDepth}
      format={'vec2<f32>'}
      label="timeBlock"
    />,
    <ComputeBuffer
      width={blockWidth}
      height={blockHeight}
      depth={blockDepth}
      format={'vec2<f32>'}
      label="freqBlock"
    />,

    <Target
      width={width}
      height={height}
      depth={depth}
      space={space}
      time={time}
      temporal={temporal}
      spaceRange={spaceRange}
      timeRange={timeRange}
    />,
    sparse ? (
      <WindowedTarget
        width={blockWidth}
        height={blockHeight}
        depth={blockDepth}
        space={space}
        time={time}
        temporal={temporal}
        spaceRange={spaceRange}
        timeRange={timeRange}
      />
    ) : null,
  ], [width, height, depth, space, time, temporal, spaceRange, timeRange, blockSize, sparse]);

  const view = useMemo(() => (<>
    <Gather
      children={buffers}
      then={([
        timeBuffer,
        freqBuffer,
        gradBuffer,

        freqTarget,
        freqBest,

        errorBuffer,
        statsBuffer,

        timeBlock,
        freqBlock,

        targetFn,
        windowedTargetFn,
      ]: [
        StorageTarget,
        StorageTarget,
        StorageTarget,

        StorageTarget,
        StorageTarget,

        StorageTarget,
        StorageTarget,

        StorageTarget,
        StorageTarget,

        TargetFn,
        TargetFn | null,
      ]) => (<>
        <Compute>
          <Suspense>
            <Score
              timeBuffer={timeBuffer}
              freqBuffer={freqBest}
              errorBuffer={errorBuffer}

              freqTarget={freqTarget}
              targetFn={targetFn}

              statsBuffer={statsBuffer}
              batch={batchSize}
              version={inited}

              tickRef={tickRef}

              onScore={handleScore}
              onCount={handleCount}
            />
          </Suspense>
        </Compute>
        <ComputeLoop
          initial
          live={running}
          batch={batchSize}
          children={
            <Suspense>
              { gradient ? (
                <Gradient
                  timeBuffer={timeBuffer}
                  freqBuffer={freqBuffer}
                  gradBuffer={gradBuffer}

                  batch={128}
                  targetFn={targetFn}
                  version={inited}
                />
              ) : null }
              <Swapper
                timeBuffer={timeBuffer}
                freqBuffer={freqBuffer}
                gradBuffer={gradBuffer}

                targetFn={sparse ? windowedTargetFn! : targetFn}
                statsBuffer={statsBuffer}

                timeBlock={timeBlock}
                freqBlock={freqBlock}

                batch={batchSize}
                block={blockSize}
                version={inited}
                gradient={gradient}
              />
            </Suspense>
          }
          then={(tick: number) => {
            let {current: count} = autoRef;
            const {current: time} = timeRef;

            if (autoBatch) {
              const now = +new Date();
              const delta = now - time;

              const n = BATCH_SIZES.length;
              const index = BATCH_SIZES.indexOf(batchSize);
              if (index < n - 1 && (delta < BATCH_FPS/2)) count++;
              else if (index > 0     && (delta > BATCH_FPS*2)) count--;
              else if (count) count -= Math.sign(count);

              if (Math.abs(count) > BATCH_AUTO) {
                if (count >  BATCH_AUTO) updateOptions({ batchSize: BATCH_SIZES[index + 1] });
                if (count < -BATCH_AUTO) updateOptions({ batchSize: BATCH_SIZES[index - 1] });
                autoRef.current = 0;
              }
              else {
                autoRef.current = count;
              }
            }

            setTick(tick);
            timeRef.current = now;
          }}
        />
        <Loop>
          <Wrapper>
            <Visualization
              timeBuffer={timeBuffer}
              freqBest={freqBest}
              freqTarget={freqTarget}
              errorBuffer={errorBuffer}
              gradBuffer={gradBuffer}

              timeBlock={timeBlock}
              freqBlock={freqBlock}

              targetFn={targetFn}
              windowedTargetFn={windowedTargetFn}

              width={width}
              height={height}
              depth={depth}
              blockWidth={blockWidth}
              blockHeight={blockHeight}
              blockDepth={blockDepth}
              boxScale={boxScale}
              tileScale={tileScale}
              gradient={gradient}
              zSlices={zSlices}
              wordcel={wordcel}
            />
          </Wrapper>
        </Loop>
        <GenControls
          inited={inited}
          running={running}
          options={options}
          display={display}
          onOptionsChange={updateOptions}
          onDisplayChange={updateDisplay}
          onDefaults={!isDefaults ? handleDefaults : undefined}
          onResize={handleResize}
          onGenerate={() => handleGenerate(timeBuffer)}
          onLoad={(blob: Blob) => handleLoad(timeBuffer, blob)}
          onSave={() => handleSave(timeBuffer)}
          onStartStop={handleStartStop}
        />
      </>)}
    />
  </>), [
    inited,
    running,
    batchSize,
    width,
    height,
    depth,
    blockWidth,
    blockHeight,
    blockDepth,
    valid,
    gamma,
    zSlices,
    wordcel,
    options,
    display,
    updateOptions,
    updateDisplay,
    handleGenerate,
    handleSave,
    handleStartStop,
    boxScale,
    tileScale,
  ]);

  return [
    <Signal />,
    view,
    <GenStatsPanel stats={stats} valid={valid} />
  ];
};

type VisualizationProps = {
  timeBuffer: StorageTarget,
  freqBest: StorageTarget,
  freqTarget: StorageTarget,
  errorBuffer: StorageTarget,
  gradBuffer: StorageTarget,
  timeBlock: StorageTarget,
  freqBlock: StorageTarget,
  targetFn: TargetFn,
  windowedTargetFn: TargetFn | null,
  width: number,
  height: number,
  depth: number,
  blockWidth: number,
  blockHeight: number,
  blockDepth: number,
  boxScale: number,
  tileScale: number,
  gradient: boolean,
  zSlices: boolean,
  wordcel: boolean,
};

const Visualization = memo((props: VisualizationProps) => {
  const {
    timeBuffer,
    freqBest,
    freqTarget,
    errorBuffer,
    gradBuffer,
    timeBlock,
    freqBlock,
    targetFn,
    windowedTargetFn,
    width,
    height,
    depth,
    blockWidth,
    blockHeight,
    blockDepth,
    boxScale,
    tileScale,
    gradient,
    zSlices,
    wordcel,
  } = props;

  const hasZ = depth > 1;
  const layer = hasZ && !zSlices ? (depth / 2)|0 : 0;
  const Frame = hasZ && zSlices ? DataFrames : DataFrame;

  const valueNorm = Math.sqrt(width * height * depth);
  const blockNorm = Math.sqrt(blockWidth * blockHeight * blockDepth);
  const {curves} = targetFn;

  const [zooming, setZooming] = useState(false);
  useResource((dispose) => {
    const onKey = (event: KeyboardEvent) => setZooming(event.altKey);

    window.addEventListener('keydown', onKey);
    window.addEventListener('keyup', onKey);
    dispose(() => {
      window.removeEventListener('keydown', onKey);
      window.removeEventListener('keyup', onKey);
    });
  });

  const [layoutWidth, setLayoutWidth] = useState(0);
  const [layoutHeight, setLayoutHeight] = useState(0);
  const layoutRef = useCallback(([x, y, w, h]: Rectangle) => {
    setLayoutWidth(w);
    setLayoutHeight(h);
  });

  const renderedCurves = (
    <Flex direction="x" anchor={"center"} align={"center"} width={'100%'} gap={10} margin={10} wrap={true}>
    {
      curves.map((curve, i) => {
        const label = i ? 'Time curve' : 'Space curve';
        return (
          <Block>
            <Block width={Math.min(512, boxScale) * 492 / 512} aspect={2} margin={10}>
              <Embed>
                <Embedded normalize>
                  <Cartesian
                    range={[[-128, 128], [0, 1]]}
                    scale={[1, -1, 1]}
                  >
                    <Grid
                      first={{unit: 256, divide: 5, end: true}}
                      second={{end: true}}
                    />
                    <Axis
                      axis="x"
                      width={3}
                    />
                    <Axis
                      axis="y"
                      width={3}
                    />
                    <Sampled
                      axes='x'
                      format='vec2<f32>'
                      size={[256]}
                      index
                      expr={(emit, x, i) => emit(x, curve[i])}
                    >
                      <Line
                        width={4}
                        color={"#60b0ff"}
                      />
                    </Sampled>
                    <Sampled
                      axes='x'
                      format='vec2<f32>'
                      size={[256]}
                      index
                      items={2}
                      expr={(emit, x, i) => {
                        emit(x, 0);
                        emit(x, curve[i]);
                      }}
                    >
                      <Surface
                        color={"#3090ff40"}
                        shaded={false}
                        mode='transparent'
                      />
                    </Sampled>
                  </Cartesian>
                </Embedded>
              </Embed>
            </Block>
            <TextLabel>{label}</TextLabel>
          </Block>
        );
      })
    }
    </Flex>
  );

  const renderedTarget = (<>
    <Frame
      source={freqTarget}
      label="Target spectrum"
      width={width}
      height={height}
      layer={layer}
      min={0}
      max={targetFn.norm}
      shift={0.5}
      zSlices={zSlices}
      scale={tileScale}
    />
  </>);

  const renderedWindowedTarget = windowedTargetFn ? (<>
    <Frame
      source={timeBlock}
      label="Sparse time domain"
      width={blockWidth}
      height={blockHeight}
      layer={0}
      min={0}
      max={1}
      zSlices={zSlices}
      scale={tileScale}
      zoom={2}
    />
    <Frame
      source={freqBlock}
      label="Sparse frequency domain"
      width={blockWidth}
      height={blockHeight}
      layer={0}
      min={0}
      max={blockNorm}
      shift={0.5}
      zSlices={zSlices}
      scale={tileScale}
      zoom={2}
    />
    <Frame
      key={blockWidth}
      source={windowedTargetFn.source}
      label="Sparse target spectrum"
      width={blockWidth}
      height={blockHeight}
      layer={layer}
      min={0}
      max={windowedTargetFn.norm}
      shift={0.5}
      zSlices={zSlices}
      scale={tileScale}
      zoom={2}
    />
  </>) : null;

  const renderedResult = (<>
    <Frame
      source={timeBuffer}
      label="Time domain"
      width={width}
      height={height}
      layer={layer}
      absolute
      min={-1}
      scale={tileScale}
    />
  </>);

  const renderedState = (<>
    <Frame
      source={freqBest}
      label="Frequency domain"
      width={width}
      height={height}
      layer={layer}
      min={0}
      max={valueNorm}
      shift={0.5}
      zSlices={zSlices}
      scale={tileScale}
    />
    <Frame
      source={errorBuffer}
      label="Spectral error"
      width={width}
      height={height}
      layer={layer}
      min={0.001}
      max={1000}
      shift={0.5}
      zSlices={zSlices}
      logarithmic
      scale={tileScale}
      colorize
    />
    <Frame
      source={freqBest}
      label="Phase"
      width={width}
      height={height}
      layer={layer}
      min={-π}
      max={π}
      shift={0.5}
      zSlices={zSlices}
      phase
      colorize
      scale={tileScale}
    />
  </>);

  const renderedGradient = (
    <Frame
      source={gradBuffer}
      label="Gradient (+/–)"
      width={width}
      height={height}
      layer={layer}
      min={-valueNorm}
      max={valueNorm}
      zSlices={zSlices}
      absolute
      scale={tileScale}
    />
  );

  const view = (
    <Pass>
      <UI>
        <Layout>
          <Absolute width="100%" height={"100%"}>
            <Block ref={layoutRef}>

              { wordcel ? (<>
                <Flex direction="x" anchor={"center"} align={"center"} width={'100%'} gap={10} margin={10} wrap={true}>

                  <Block width={Math.min(1024, boxScale * 2)} margin={[0, 20, 0, 0]}>
                    <TextFlow>Design a target spectrum based on a space and/or time curve:</TextFlow>
                  </Block>

                </Flex>

                <Flex direction="x" anchor={"center"} align={"center"} width={'100%'} gap={10} margin={10} wrap={true}>
                  {renderedCurves}
                </Flex>

                <Flex direction="x" anchor={"center"} align={"center"} width={'100%'} gap={10} margin={10} wrap={true}>
                  {renderedTarget}
                </Flex>


                {windowedTargetFn ? (<>

                  <Flex direction="x" anchor={"center"} align={"center"} width={'100%'} gap={10} margin={10} wrap={true}>

                    <Block width={Math.min(1024, boxScale * 2)} margin={[0, 20, 0, 0]}>
                      <TextFlow>Optimize a uniform and equally distributed noise texture by swapping pairs of pixels:</TextFlow>
                    </Block>

                  </Flex>

                  <Flex direction="x" anchor={"center"} align={"center"} width={'100%'} gap={10} margin={10} wrap={true}>
                    {renderedResult}
                  </Flex>

                  <Flex direction="x" anchor={"center"} align={"center"} width={'100%'} gap={10} margin={10} wrap={true}>

                    <Block width={Math.min(1024, boxScale * 2)} margin={[0, 20, 0, 0]}>
                      <TextFlow>To search approximately, sparse scoring will only score a spherical region around each change:</TextFlow>
                    </Block>

                  </Flex>

                  <Flex direction="x" anchor={"center"} align={"center"} width={'100%'} gap={10} margin={10} wrap={true}>
                    {renderedWindowedTarget}
                  </Flex>

                  <Flex direction="x" anchor={"center"} align={"center"} width={'100%'} gap={10} margin={10} wrap={true}>
                    <Block width={Math.min(1024, boxScale * 2)} margin={[0, 20, 0, 0]}>
                      <TextFlow>The target spectrum converges to roughly the same result:</TextFlow>
                    </Block>
                  </Flex>

                  <Flex direction="x" anchor={"center"} align={"center"} width={'100%'} gap={10} margin={10} wrap={true}>
                    {renderedState}
                  </Flex>
                </>) : (<>

                  <Flex direction="x" anchor={"center"} align={"center"} width={'100%'} gap={10} margin={10} wrap={true}>

                    <Block width={Math.min(1024, boxScale * 2)} margin={[0, 20, 0, 0]}>
                      <TextFlow>Optimize a uniform and equally distributed noise texture by swapping pairs of pixels:</TextFlow>
                    </Block>

                  </Flex>

                  <Flex direction="x" anchor={"center"} align={"center"} width={'100%'} gap={10} margin={10} wrap={true}>
                    {renderedResult}
                    {renderedState}
                  </Flex>


                </>)}

                { gradient ? (<>
                  <Flex direction="x" anchor={"center"} align={"center"} width={'100%'} gap={10} margin={10} wrap={true}>
                    <Block width={Math.min(1024, boxScale * 2)} margin={[0, 20, 0, 0]}>
                      <TextFlow>The time-domain gradient of the spectral error can be used to identify likely beneficial swaps:</TextFlow>
                    </Block>
                  </Flex>
                  <Flex direction="x" anchor={"center"} align={"center"} width={'100%'} gap={10} margin={10} wrap={true}>
                    {renderedGradient}
                  </Flex>
                </>
                ) : null}
              </>) : (<>
                <Flex direction="x" anchor={"center"} align={"center"} width={'100%'} gap={10} margin={10} wrap={true}>
                  {renderedCurves}
                </Flex>
                { windowedTargetFn ? (<>
                  <Flex direction="x" anchor={"center"} align={"center"} width={'100%'} gap={10} margin={10} wrap={true}>
                    {renderedTarget}
                    {renderedResult}
                  </Flex>
                  <Flex direction="x" anchor={"center"} align={"center"} width={'100%'} gap={10} margin={10} wrap={true}>
                    {renderedWindowedTarget}
                  </Flex>
                  <Flex direction="x" anchor={"center"} align={"center"} width={'100%'} gap={10} margin={10} wrap={true}>
                    {renderedState}
                    {gradient ? renderedGradient : null}
                  </Flex>
                </>) : (<>
                  <Flex direction="x" anchor={"center"} align={"center"} width={'100%'} gap={10} margin={10} wrap={true}>
                    {renderedResult}
                    {renderedTarget}
                    {renderedState}
                    {gradient ? renderedGradient : null}
                  </Flex>
                </>) }
              </>)}

              <Block height={20} />
            </Block>
          </Absolute>
        </Layout>
      </UI>
    </Pass>
  );

  const [x, setX] = useState(0);
  const [y, setY] = useState(0);
  const [zoom, setZoom] = useState(1);
  const [panVersion, setPanVersion] = useState(0);

  return (
    <PanControls
      active={layoutHeight}
      scroll
      soft
      minZoom={1}
      maxZoom={6}
      minX={0}
      minY={0}
      maxX={layoutWidth}
      maxY={layoutHeight}
      x={x}
      y={y}
      zoom={zoom}
      version={panVersion}
      zoomSpeed={1/300}
      snapZoom={0.1}
      render={(x, y, zoom, ox, oy) => {
        setX(ox);
        setY(oy);
        setZoom(zoom);

        return (<>
          <Zoom
            min={1}
            max={6}
            value={zoom}
            onChange={(zoom: number) => {
              setZoom(zoom);
              setPanVersion(v => v + 1);
            }}
          />
          <Flat x={x} y={y} zoom={zoom} focus={1}>
            {view}
            <Cursor cursor="move" />
          </Flat>
        </>);
      }}
    />
  );
}, 'Visualization');

type DataFrameProps = {
  source: ShaderSource,
  label: string,
  width: number,
  height: number,
  scale?: number,
  min?: number,
  max?: number,
  shift?: number,
  layer?: number,
  zSlices?: boolean,
  phase?: boolean,
  logarithmic?: boolean,
  absolute?: boolean,
  zoom?: number,
};

const DataFrames = (props: DataFrameProps) => {
  const {key, ...rest} = props;
  const {source, label, scale, zoom = 1, shift = 0} = props;
  const {size: [width, height, depth]} = source;

  const gx = Math.min(Math.min(depth, 8), Math.pow(2, Math.ceil(Math.log2(depth) / 2) + 1));
  const gy = Math.ceil(depth / gx);
  const tileScale = Math.min(Math.max(width, height) * 2, 2 * scale / gx / zoom) * zoom;

  const g = gx * gy;
  const getLayer = (x, y) => ((x + y * gx) + Math.floor(shift * g)) % g;

  const aspectW = Math.min(1, width / height);
  const aspectH = Math.min(1, height / width);

  return (
    <Block>
      {seq(gy).map(y => (
        <Block width={gx * tileScale * aspectW} height={tileScale * aspectH} direction="x">
          {seq(gx).map(x => <DataFrame {...rest} label={null} scale={tileScale * aspectH} layer={getLayer(x, y)} />)}
        </Block>
      ))}
      {label != null ? <TextLabel>{label}</TextLabel> : null}
    </Block>
  );
};

const DataFrame = (props: DataFrameProps) => {
  const {
    source,
    label,
    width,
    height,
    scale = 512,
    min = 0,
    max = 1,
    layer = 0,
    shift = 0,
    colorize = false,
    zSlices = true,
    phase = false,
    logarithmic = false,
    absolute = false,
  } = props;

  const aspect = width / height;
  let s = scale;
  if (s * aspect > 512) s = 512 / aspect;

  const {size: [w, h]} = source;

  return (
    <DataShader
      source={source}
      shader={absolute ? visualizeValue : phase ? visualizePhase : visualizeMagnitude}
      args={[layer, min, max, shift, colorize, logarithmic]}
      render={(texture: ShaderSource) =>
        <DataShader
          source={texture}
          shader={visualizeAA}
          args={[[1/w, 1/h]]}
          render={(texture: ShaderSource) => {
            const image = useOne(() => ({
              texture,
              fit: 'scale',
              repeat: 'none',
            }), texture);

            return (
              <Block>
                <Block
                  border={Math.min(width, height) > 1 ? 1 : 0}
                  stroke={GRAY}
                  aspect={aspect}
                  height={s}
                  fill={GRAY}
                  image={image}
                />
                {label != null ? <Inline align="center"><Text size={16} lineHeight={36} color={WHITE}>{label}</Text></Inline> : null}
              </Block>
            );
          }}
        />
      }
    />
  );
}

type TextLabelProps = {
  children: any,
};

const TextLabel = (props: TextLabelProps) => {
  const {children} = props;
  return children != null ? <Inline align="center"><Text size={16} lineHeight={36} color={WHITE}>{children}</Text></Inline> : null;
};

const TEXT_FLOW_MARGIN = [0, 4];
const TextFlow = (props: TextLabelProps) => {
  const {children} = props;
  return children != null ? <Inline margin={TEXT_FLOW_MARGIN} align="left"><Text size={16} weight="bold" lineHeight={24} color={WHITE}>{children}</Text></Inline> : null;
};
