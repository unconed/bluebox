import type { Update } from '@use-gpu/state';
import type { GenState } from './gen/types';

import React from 'react';
import { use, useResource, useState, LC } from '@use-gpu/live';
import { HTML } from '@use-gpu/react';
import { seq } from '@use-gpu/core';
import { fmts } from '../util';

import { CheckboxInput } from '../input/CheckboxInput';
import { NumberInput } from '../input/NumberInput';
import { SelectInput } from '../input/SelectInput';
import { SliderInput } from '../input/SliderInput';
import { Tooltip, Help } from '../html/Tooltip';

type GenControlsProps = {
  inited: boolean,
  running: boolean,
  options: GenState,
  display: GenDisplay,
  onDefaults?: () => void,
  onSparse?: (sparse: boolean) => void,
  onResize?: (w: number, h: number, d: number) => void,
  onOptionsChange?: (u: Update<GenState>) => void,
  onDisplayChange?: (u: Update<GenDisplay>) => void,
  onGenerate?: () => void,
  onLoad?: (image: Image) => void,
  onSave?: () => void,
};

const SIZE_OPTIONS = seq(9).map(i => `${1 << i}`).map(s => ({value: s, label: s}));

export const BLOCK_SIZES = [8, 16, 32, 64, 128, 256];
export const BATCH_SIZES = [1, 2, 4, 7, 10, 20, 40, 70, 100, 200, 400, 700, 1000, 2000, 4000, 7000, 10000];

const BLOCK_OPTIONS = BLOCK_SIZES.map(s => ({value: s, label: s.toString()}));
const BATCH_OPTIONS = BATCH_SIZES.map(s => ({value: s, label: s.toString()}));
const CURVE_OPTIONS = ['linear', 'cosine', 'cosineQ', 'complement'].map(s => ({value: s, label: s}));

const DROP_ZONE: CSSProperties = {
  position: 'absolute',

  left: 0,
  top: 0,
  right: 0,
  bottom: 0,

  zIndex: 50,
  pointerEvents: 'none',
};

const DROP_MARKER: CSSProperties = {
  position: 'absolute',

  width: '70%',
  height: '70%',
  left: '50%',
  top: '50%',
  transform: 'translate(-50%, -50%)',

  borderRadius: 40,
  border: '10px dashed #FFFFFF80',
  background: '#FFFFFF40',

  zIndex: 60,
  pointerEvents: 'none',
};

export const GenControls: LC<GenControlsProps> = (props: GenControlsProps) => {
  const {
    inited,
    running,
    options,
    display,
    onOptionsChange,
    onDisplayChange,
    onDefaults,
    onResize,
    onGenerate,
    onStartStop,
    onLoad,
    onSave,
  } = props;

  const {width, height, depth, space, time, temporal, spaceRange, timeRange, gradient, sparse, autoBatch, batchSize, blockSize} = options;
  const {gamma, zSlices, wordcel} = display;
  const root = document.querySelector('.control-panel .controls');

  const hasZ = depth > 1;
  
  const maxS = Math.max(width, height, depth);
  const resolvedBlockSize = Math.min(Math.ceil(maxS / 2), blockSize);
  const blockOptions = BLOCK_OPTIONS.filter(({value}) => value < maxS);

  const [dragging, setDragging] = useState(false);

  useResource((dispose) => {
    const canvas = document.querySelector('canvas');
    if (!canvas) return;

    const handleDragOver = (e: any) => {
      e.preventDefault();
      setDragging(true);
    };

    const handleDragLeave = (e: any) => {
      e.preventDefault();
      setDragging(false);
    };

    const handleDrop = (e: any) => {
      e.preventDefault();
      if (!e.dataTransfer.files) return;

      const {dataTransfer: {files}} = e;
      const [file] = files;
      if (!file) return;

      const r = new FileReader();
      console.log(r)
      r.onload = (e) => {
        if (!e.target) return;

        const src = e.target.result as ArrayBuffer;
        const i = new Image();
        i.onload = (e) => {
          return onLoad(i);
        };
        i.src = src;
      };
      r.readAsDataURL(file);

      setDragging(false);
    };

    canvas.addEventListener('dragover', handleDragOver);
    canvas.addEventListener('dragleave', handleDragLeave);
    canvas.addEventListener('drop', handleDrop);
    dispose(() => {
      canvas.removeEventListener('dragover', handleDragOver);
      canvas.removeEventListener('dragleave', handleDragLeave);
      canvas.removeEventListener('drop', handleDrop);
    });
  }, [onLoad]);
  
  const hasTime = temporal && hasZ;
  const timeStyle = {opacity: hasTime ? 1 : 0.5};
  const tooSmall = width * height * depth < 256;

  return [
    use(HTML, {
      container: document.body,
      style: DROP_ZONE,
      children: (<>
        {dragging ? <div style={DROP_MARKER} /> : null}
      </>)
    }),
    use(HTML, {
      container: root,
      style: {
        display: 'flex'
      },
      children: (<>
        <Panel style={{width: 460}}>
          <TitleBar>Stable Fiddusion 1.0</TitleBar>
          <InfoLink><a href="https://acko.net/blog/stable-fiddusion/" target="_blank">Info</a></InfoLink>

          <Header>Target</Header>
          <Row>
            <Label>Size</Label>
            <SelectInput
              width={60}
              options={SIZE_OPTIONS}
              value={`${width}`}
              onChange={(width) => {
                onOptionsChange({width: +width});
                onResize(width, height, depth, sparse);
              }}
            />
            {" × "}
            <SelectInput
              width={60}
              options={SIZE_OPTIONS}
              value={`${height}`}
              onChange={(height) => {
                onOptionsChange({height: +height});
                onResize(width, height, depth, sparse);
              }}
            />
            {" × "}
            <SelectInput
              width={60}
              options={SIZE_OPTIONS}
              value={`${depth}`}
              onChange={(depth) => {
                onOptionsChange({depth: +depth});
                onResize(width, height, depth, sparse);
              }}
            />
            {tooSmall ? <span style={{lineHeight: '8px', marginLeft: 10}}>⚠️</span> : ''}
            <Grow />
            {onDefaults ? <div style={{margin: '-4px 0'}}><Tooltip x={-10} y={-5} label="Reset to defaults"><Button onClick={onDefaults}><Reset /></Button></Tooltip></div> : null}
          </Row>
          <label>
            <Row>
              <Label>Spatio-temporal</Label>
              <CheckboxInput disabled={!hasZ} value={temporal} onChange={(temporal) => onOptionsChange({temporal})} />
            </Row>
          </label>
          <Row>
            <Label>Space curve</Label>
            <SelectInput value={space} options={CURVE_OPTIONS} onChange={(space) => onOptionsChange({space})} />
            <Space />
            <Value>{fmts(spaceRange[0], 2)}</Value>
            <SliderInput min={0} max={1} step={0.05} width={80} value={spaceRange} onChange={(spaceRange) => onOptionsChange({spaceRange})} />
            <Value>{fmts(spaceRange[1], 2)}</Value>
          </Row>
          <Row>
            <Label style={timeStyle}>Time curve</Label>
            <SelectInput disabled={!hasTime} value={time} options={CURVE_OPTIONS} onChange={(time) => onOptionsChange({time})} />
            <Space />
            <Value style={timeStyle}>{fmts(timeRange[0], 2)}</Value>
            <SliderInput disabled={!hasTime} min={0} max={1} step={0.05} width={80} value={timeRange} onChange={(timeRange) => onOptionsChange({timeRange})} />
            <Value style={timeStyle}>{fmts(timeRange[1], 2)}</Value>
          </Row>
        </Panel>

        <Panel style={{width: 320}}>
          <Header>Generate</Header>
          <Row>
            <Label><label htmlFor="gradient">Gradient boost</label></Label>
            <CheckboxInput id="gradient" value={gradient} onChange={(gradient) => onOptionsChange({gradient})} />
            <Grow />
            <Tooltip x={-20} y={-9} label="Use gradient of spectral error to improve sample selection">
              <Help />
            </Tooltip>
          </Row>
          <Row>
            <Label><label htmlFor="sparse">Sparse scoring</label></Label>
            <CheckboxInput
              id="sparse"
              value={sparse}
              onChange={(sparse) => {
                onOptionsChange({sparse});
                onResize(width, height, depth, sparse);
              }}
            />
            {sparse ? (<>
              <SelectInput disabled={!sparse} value={blockSize} options={blockOptions} onChange={(blockSize) => onOptionsChange({blockSize: +blockSize})} />
            </>) : null}
            <Grow />
            <Tooltip x={-20} y={-9} label="Score only NxNxN pixels around each swap. Fast but approximate.">
              <Help />
            </Tooltip>
          </Row>
          <Row>
            <Label><label htmlFor="autoBatch">Batch size</label></Label>
            <CheckboxInput
              id="autoBatch"
              value={!autoBatch}
              onChange={(autoBatch) => {
                onOptionsChange({autoBatch: !autoBatch});
              }}
            />
            <SelectInput disabled={autoBatch} value={batchSize} options={BATCH_OPTIONS} onChange={(batchSize) => onOptionsChange({batchSize: +batchSize})} />
            <Grow />
            <Tooltip x={-20} y={-9} label="Size of dispatch to GPU (or automatic)">
              <Help />
            </Tooltip>
          </Row>
          <div style={{marginTop: 35, marginBottom: -5}}>
            {
              inited ? (<Row>
                <Button disabled={tooSmall} onClick={onStartStop} $primary style={{width: 100}}>{running ? <Stop /> : <Play />} {running ? 'Pause' : 'Resume'}</Button>
                <Button onClick={onSave}>Save PNG</Button>
                <Grow />
                <Button $danger onClick={onGenerate}><Trash /> Reset</Button>
              </Row>) : (<Row>
                <Button disabled={tooSmall} $primary onClick={onGenerate}><Play />{' Generate'}</Button>
              </Row>)
            }
          </div>
        </Panel>

        <Panel style={{width: 200}}>
          <Header>Display</Header>
          <label>
            <Row>
              <Label>Gamma correct</Label>
              <CheckboxInput value={gamma} onChange={(gamma) => onDisplayChange({gamma})} />
            </Row>
          </label>
          <label>
            <Row>
              <Label>Z slices</Label>
              <CheckboxInput disabled={!hasZ} value={zSlices} onChange={(zSlices) => onDisplayChange({zSlices})} />
            </Row>
          </label>
          <label>
            <Row>
              <Label>Explanation</Label>
              <CheckboxInput value={wordcel} onChange={(wordcel) => onDisplayChange({wordcel})} />
            </Row>
          </label>
          <Info style={{marginTop: 20}}>
            • Saved PNGs are linear RGB
            • Drag and drop PNG to load
            • ALT+Scroll to zoom
          </Info>
        </Panel>
      </>),
    }),
  ];
};

export const Play = (props) => <div style={{
  ...props.style,
  display: 'inline-block',
  width: 10,
  height: 5,
  marginRight: 4,
  borderLeft: `10px solid ${props.color ?? '#fff'}`,
  borderTop: `5px solid transparent`,
  borderBottom: `5px solid transparent`,
}} />;

export const Stop = (props) => <div style={{
  ...props.style,
  display: 'inline-block',
  width: 10,
  height: 8,
  position: 'relative',
  top: -1,
  marginRight: 4,
  borderLeft: `4px solid ${props.color ?? '#fff'}`,
  borderRight: `4px solid ${props.color ?? '#fff'}`,
}} />;

export const Trash = (props) => (
  <div style={{
    ...props.style,
    display: 'inline-block',
    width: 14,
    height: 14,
    lineHeight: 15,
    position: 'relative',
    verticalAlign: 'middle',
    top: -1,
    left: -4,
  }}>
    <div style={{
      position: 'absolute',
      left: 5,
      top: -1,
      width: 4,
      height: 15,
      background: '#fff',
      transform: 'rotate(45deg)',
    }} />
    <div style={{
      position: 'absolute',
      left: 5,
      top: -1,
      width: 4,
      height: 15,
      background: '#fff',
      transform: 'rotate(-45deg)',
    }} />
  </div>
);

export const Reset = (props) => (
  <div style={{
    ...props.style,
    display: 'inline-block',
    width: 14,
    height: 14,
    lineHeight: 10,
    position: 'relative',
    verticalAlign: 'middle',
    top: -2,
    left: -1,
  }}>
    <div style={{
      position: 'absolute',
      left: 0,
      top: 0,
      width: 16,
      height: 16,
      border: '3px solid #fff',
      borderRadius: '100%',
      clipPath: 'polygon(7px 7px, -3px 0px, 16px 0px, 16px 16px, -3px 16px)',
    }} />
    <div style={{
      position: 'absolute',
      left: -1,
      top: 2,
      width: 6,
      height: 4,
      borderLeft: '6px solid #fff',
      borderTop: '4px solid transparent',
      borderBottom: '4px solid transparent',
      transform: 'rotate(125deg)',
    }} />
  </div>
);

export const Space = (props) => <div style={{...props.style, width: 10, height: 10}}>{props.children}</div>;
export const Grow = (props) => <div style={{...props.style, flexGrow: 1}}>{props.children}</div>;

export const Row = (props) => <div style={{...props.style, height: '30px', display: 'flex', alignItems: 'center', padding: '4px 0px'}}>{props.children}</div>;
export const Label = (props) => <div style={{...props.style, fontSize: 14, minWidth: 130}}>{props.children}</div>;
export const Text = (props) => <div style={{...props.style, fontSize: 14}}>{props.children}</div>;
export const Header = (props) => <div style={{...props.style, fontSize: 14, textTransform: 'uppercase', fontWeight: 'bold', marginBottom: 5}}>{props.children}</div>;
export const Panel = (props) => <div style={{
  ...props.style,
  position: 'relative',
  padding: 20,
  borderRight: '1px solid rgba(255, 255, 255, .35)',
}}>{props.children}</div>;

export const Info = (props) => <div style={{...props.style, fontSize: 12, marginLeft: 0, paddingLeft: 0}}>{props.children}</div>;
export const Value = (props) => <div style={{...props.style, fontSize: 12, marginLeft: 0, padding: '0 6px', width: 40, textAlign: 'center'}}>{props.children}</div>;

export const TitleBar = (props) => <div style={{
  ...props.style,
  position: 'absolute',
  bottom: 20,
  left: 20,
  fontSize: 16,
  textTransform: 'uppercase',
  fontWeight: 900,
  letterSpacing: '2px',
  color: '#666',
}}>{props.children}</div>;

export const InfoLink = (props) => <div style={{
  ...props.style,
  position: 'absolute',
  bottom: 20,
  right: 20,
  fontSize: 14,
  textTransform: 'uppercase',
  fontWeight: 900,
  letterSpacing: '2px',
  opacity: 0.8,
}}>{props.children}</div>;

export const Button = (props) => {
  const {$primary, $secondary, $danger, ...rest} = props;

  return (
    <button
      {...rest}
      style={{
        ...props.style,
        cursor: 'pointer',
        padding: '6px 12px',
        fontWeight: 'bold',
        color: (
          $primary ? '#fff' :
          $secondary ? '#fff' :
          $danger ? '#fff' :
          '#fff'
        ),
        background: (
          !props.disabled ? (
            $primary ? "#3090FF" :
            $secondary ? '#7b0' :
            $danger ? '#b20' :
            "#888"
          ) : '#888'
        ),
        opacity: rest.disabled ? 0.5 : 1,
        border: 0,
        margin: '0px 2px',
        borderRadius: 4,
      }}
    />
  );
};
