import type { TypedArray } from '@use-gpu/core';
import FFT from 'fft.js';

const FFT_MAP = new Map<number, FFT>();
export const getFFT = (size: number) => {
  if (FFT_MAP.has(size)) return FFT_MAP.get(size)!;

  const fft = new FFT(size);
  FFT_MAP.set(size, fft);
  return fft;
}

const _vs = new Float32Array(1024);
const _fs = new Float32Array(1024);

export const fft3d = (vs: TypedArray, fs: TypedArray, size: number[], options?: FFTOptions) => {
  const [w, h, d] = size;
  const wh = w * h;
  const whd = wh * d;

  const xx = !!(options?.x ?? true);
  const yy = !!(options?.y ?? true);
  const zz = !!(options?.z ?? true);

  const fftX = getFFT(Math.max(2, w));
  const fftY = getFFT(Math.max(2, h));
  const fftZ = getFFT(Math.max(2, d));
  
  const op = options?.inverse ? 'inverseTransform' : 'transform';

  copyComplexArrayRange(vs, fs, 0, 0, whd, 1, 1);

  if (xx && w > 1) {
    for (let z = 0; z < d; ++z) {
      for (let y = 0; y < h; ++y) {
        let base = (z * h + y) * w;
        copyComplexArrayRange(fs, _vs, base, 0, w, 1, 1);
        fftX[op](_fs, _vs);
        copyComplexArrayRange(_fs, fs, 0, base, w, 1, 1);
      }
    }
  }

  if (yy && h > 1) {
    for (let z = 0; z < d; ++z) {
      for (let x = 0; x < w; ++x) {
        let base = (z * h * w) + x;
        copyComplexArrayRange(fs, _vs, base, 0, h, w, 1);
        fftY[op](_fs, _vs);
        copyComplexArrayRange(_fs, fs, 0, base, h, 1, w);
      }
    }
  }
  
  if (zz && d > 1) {
    for (let y = 0; y < h; ++y) {
      for (let x = 0; x < w; ++x) {
        let base = (y * w) + x;
        copyComplexArrayRange(fs, _vs, base, 0, d, wh, 1);
        fftZ[op](_fs, _vs);
        copyComplexArrayRange(_fs, fs, 0, base, d, 1, wh);
      }
    }
  }
};

export const copyComplexArrayRange = (
  from: TypedArray, to: TypedArray,
  fromIndex: number, toIndex: number, length: number,
  fromStride: number = 1, toStride: number = 1,
) => {
  const n = length;
  var fromBase = fromIndex * 2;
  var toBase = toIndex * 2;
  for (let i = 0; i < n; i++) {
    to[toBase    ] = from[fromBase    ];
    to[toBase + 1] = from[fromBase + 1];

    fromBase += 2 * fromStride;
    toBase += 2 * toStride;
  }
};
