import type { LC } from '@use-gpu/live';
import type { StorageSource, StorageTarget } from '@use-gpu/live';

import { use, useMemo, useOne } from '@use-gpu/live';
import { Dispatch, useBoundShader, useShaderRef } from '@use-gpu/workbench';

import { fftCopy } from '../wgsl/fft-copy.wgsl';
import { fftApply } from '../wgsl/fft-apply.wgsl';

export type FFTProps = {
  source: StorageSource,
  target: StorageTarget,
  inverse?: boolean,
  skip?: boolean,

  shouldDispatch?: any,
  onDispatch?: any,
};

const isPowerOf2 = (x: number) => !(x & (x - 1));

export const FFT: LC<FFTProps> = (props: FFTProps) => {
  const {source, target, inverse, skip, shouldDispatch, onDispatch} = props;

  const {size} = target;
  const [w, h, d] = size;

  if (!isPowerOf2(w)) throw new Error(`Non-power-of-2 FFT size ${w} not supported`);
  if (!isPowerOf2(h)) throw new Error(`Non-power-of-2 FFT size ${h} not supported`);
  if (!isPowerOf2(d)) throw new Error(`Non-power-of-2 FFT size ${d} not supported`);

  const s = useShaderRef(size);

  const defs = useOne(() => ({INVERSE: !!inverse}), inverse);
  const copyShader = (source !== target) ? useBoundShader(fftCopy, [s, target, source]) : useNoBoundShader();
  const xShader = useBoundShader(fftApply, [s, 1, target], defs);
  const yShader = useBoundShader(fftApply, [s, 2, target], defs);
  const zShader = useBoundShader(fftApply, [s, 3, target], defs);

  const dispatch = useMemo(() => [
    copyShader ? use(Dispatch, {
      size,
      group: [64, 1, 1],
      shader: copyShader,
      shouldDispatch,
      onDispatch,
    }) : null,
    (!skip && w > 1) ? use(Dispatch, {
      size: [size[1], size[2], 1],
      group: [8, 8, 1],
      shader: xShader,
      shouldDispatch,
    }) : null,
    (!skip && h > 1) ? use(Dispatch, {
      size: [size[0], size[2], 1],
      group: [8, 8, 1],
      shader: yShader,
      shouldDispatch,
    }) : null,
    (!skip && d > 1) ? use(Dispatch, {
      size: [size[0], size[1], 1],
      group: [8, 8, 1],
      shader: zShader,
      shouldDispatch,
    }) : null,
  ], [size, skip, copyShader, xShader, yShader, zShader]);

  return dispatch;
};
