import type { LC } from '@use-gpu/live';
import { yeet, useMemo } from '@use-gpu/live';

import { useBoundShader, useShaderRef } from '@use-gpu/workbench';
import { seq } from '@use-gpu/core';

import { CURVES_JS, getTargetJS, getTargetWGSL } from './curves';

export type TargetProps = {
  width: number,
  height: number,
  depth: number,
  space: string,
  time: string,
  temporal: boolean,
};

export const Target: LC<TargetProps> = (props: TargetProps) => {
  const {width, height, depth, space, time, temporal, spaceRange, timeRange} = props;

  const ts = useShaderRef(spaceRange);
  const tt = useShaderRef(timeRange);

  const [norm, curves] = useMemo(() => {
    const getSpace = CURVES_JS[space];
    const getTime = CURVES_JS[time];
    
    const getTarget = getTargetJS(space, time, temporal, spaceRange, timeRange, depth);

    const ix = 2/width;
    const iy = 2/height;
    const iz = 2/depth;

    let i = 0;
    let accum = 0;
    for (let z = 0; z < depth; z++) {
      const dz = z * iz;
      const fz = Math.min(dz, 2 - dz);
      for (let y = 0; y < height; y++) {
        const dy = y * iy;
        const fy = Math.min(dy, 2 - dy);
        for (let x = 0; x < width; x++) {
          const dx = x * ix;
          const fx = Math.min(dx, 2 - dx);
          accum += getTarget(fx, fy, fz);
        }
      }
    }

    const detail = 128;
    const n = detail * 2 + 1;

    const curves = [
      seq(n).map(i => getSpace(Math.abs((i - detail) / detail), spaceRange)),
      depth > 1 && temporal ? seq(n).map(i => getTime(Math.abs((i - detail) / detail), timeRange)) : null,
    ].filter(x => !!x);

    const norm = width * height * depth / accum;

    return [norm, curves];
  }, [width, height, depth, space, time, temporal, spaceRange, timeRange]);

  const shader = getTargetWGSL(space, time, temporal, spaceRange, timeRange, depth, norm);

  return useMemo(() => yeet({shader, norm, curves}), [shader, norm, spaceRange, timeRange, curves]);
};
