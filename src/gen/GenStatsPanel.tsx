import type { GenStats } from './gen/types';

import React from 'react';
import { memo, use, useOne, LC } from '@use-gpu/live';
import { HTML } from '@use-gpu/react';
import { Tooltip, Help } from '../html/Tooltip';

import { fmt, fmts } from '../util';

type GenStatsPanelProps = {
  valid: boolean,
  inited: boolean,
  stats: GenStats,
};

export const GenStatsPanel: LC<GenStatsPanelProps> = memo((props: GenStatsPanelProps) => {
  const {
    valid,
    inited,
    stats,
  } = props;
  
  const {score, rate, success, total, percent, steps, speed} = stats;
  
  const root = document.querySelector('.control-panel .stats');

  return use(HTML, {
    container: root,
    style: {
    },
    children: (
      <Panel style={{width: 400}}>
        <Header>Stats</Header>
        <label>
          <Row>
            <Label>Histogram</Label>
            <Stat>{valid ? <><span>Uniform</span> <Emoji>✔️</Emoji></> : <><span>Non-uniform</span> <Emoji>❌</Emoji></>}</Stat>
            <Grow /> 
            <Tooltip x={-20} y={-9} label={"Pixel distribution must be equal over bytes 0-255"}><Help /></Tooltip>
          </Row>
          <Row>
            <Label>Steps</Label>
            <Stat>{steps} ({speed|0} / s)</Stat>
            <Grow />
            <Tooltip x={-20} y={-9} label={"Number of swaps attempted"}><Help /></Tooltip>
          </Row>
          <Row>
            <Label>Loss</Label>
            <Stat>{fmt(score, 5)}</Stat>
            <Grow />
            <Tooltip x={-20} y={-9} label={"L2 norm of spectral error (per pixel)"}><Help /></Tooltip>
          </Row>
          <Row>
            <Label>Convergence</Label>
            <Stat>{fmt(rate)} / s</Stat>
            {rate < 0 ? <span style={{lineHeight: '8px', marginLeft: 10}}>⚠️</span> : ''}

            <Grow />
            <Tooltip x={-20} y={-9} label={"Convergence rate will smoothly decrease over time"}><Help /></Tooltip>
          </Row>
          <Row>
            <Label>Success rate</Label>
            <Stat>{fmts(percent, 1)}% ({fmts(success, 1)} / {total})</Stat>
            <Grow />
            <Tooltip x={-20} y={-9} label={"Percentage of successful swaps"}><Help /></Tooltip>
          </Row>
        </label>
      </Panel>
    ),
  });
}, 'GenStatsPanel');

const MONOSPACE = 'Fira Code, Source Code Pro, Anonymous Pro, Inconsolata, Consolas, Monaco, monospace';

export const Row = (props) => <div style={{...props.style, display: 'flex', alignItems: 'center', padding: '4px 0px'}}>{props.children}</div>;
export const Label = (props) => <div style={{...props.style, fontSize: 14, minWidth: 130}}>{props.children}</div>;
export const Header = (props) => <div style={{...props.style, fontSize: 14, color: '#fff', textTransform: 'uppercase', fontWeight: 'bold', marginBottom: 5}}>{props.children}</div>;
export const Panel = (props) => <div style={{...props.style, padding: 20}}>{props.children}</div>;
export const Stat = (props) => <span style={{...props.style, whiteSpace: 'pre', fontFamily: MONOSPACE, userSelect: 'all'}}>{props.children}</span>;
export const Emoji = (props) => <span style={{...props.style, whiteSpace: 'pre', height: 0, lineHeight: 0, verticalAlign: 'middle', fontFamily: MONOSPACE, fontSize: 10}}>{props.children}</span>;
export const Grow = (props) => <div style={{...props.style, flexGrow: 1}}>{props.children}</div>;
