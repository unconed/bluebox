import type { LC } from '@use-gpu/live';
import type { Ref, StorageSource, StorageTarget } from '@use-gpu/live';
import type { ShaderSource } from '@use-gpu/shader/wgsl';
import type { TargetFn } from './types';

import { memo, yeet, use, useMemo, useOne, useVersion } from '@use-gpu/live';
import { Dispatch, Readback, useBoundShader, useDeviceContext, useDerivedSource, useScratchSource, useShaderRef } from '@use-gpu/workbench';
import { clearBuffer, uploadBuffer } from '@use-gpu/core';

import { swapMean } from '../wgsl/swap-mean.wgsl';
import { swapScore } from '../wgsl/swap-score.wgsl';
import { swapTarget } from '../wgsl/swap-target.wgsl';

import { swapApply } from '../wgsl/swap-apply.wgsl';
import { swapSparse } from '../wgsl/swap-sparse.wgsl';

import { FFT } from './FFT';
import { iir, len2 } from '../util';

export type SwapProps = {
  timeBuffer: StorageTarget,
  freqBuffer: StorageTarget,
  gradBuffer: StorageTarget,

  targetFn: TargetFn,
  statsBuffer: StorageTarget,

  timeBlock: StorageTarget,
  freqBlock: StorageTarget,

  batch: number,
  version: number,
  gradient?: boolean,
};

const isPowerOf2 = (x: number) => !(x & (x - 1));

export const SwapSparse: LC<SwapProps> = memo((props: SwapProps) => {
  const {
    timeBuffer,
    freqBuffer,
    gradBuffer,

    targetFn,
    statsBuffer,

    timeBlock,
    freqBlock,

    block = 16,
    batch = 100,
    version,
    gradient = true,
  } = props;

  const {size, length} = freqBuffer;
  const [w, h, d] = size;

  const bw = Math.min(w, block);
  const bh = Math.min(h, block);
  const bd = Math.min(d, block);

  const device = useDeviceContext();

  const blockLength = bw * bh * bd;
  const blockSize = useOne(() => [bw, bh, bd], [bw, bh, bd]);
  const blockOffset = useOne(() => [Math.floor(bw/2), Math.floor(bh/2), Math.floor(bd/2)], [bw, bh, bd]);

  const l = useShaderRef(w * h * d);
  const s = useShaderRef(size);

  const bs = useShaderRef(blockSize);
  const bo = useShaderRef(blockOffset);

  const b = useShaderRef(batch);  

  const options = useOne(() => ({
    flags: GPUBufferUsage.STORAGE | GPUBufferUsage.COPY_SRC | GPUBufferUsage.COPY_DST,
    readWrite: true,
    reserve: 16,
  }));
  const [mean] = useScratchSource('f32', options);
  const [score] = useScratchSource('f32', options);
  const [swap] = useScratchSource('u32', options);
  const [_dummy] = useScratchSource('f32', options);

  const g = useShaderRef(gradient);
  const i = useShaderRef(0);
  const s1 = useShaderRef(0);
  const s2 = useShaderRef(0);
  const k = useShaderRef(0);

  const blockCopy = timeBlock;
  const blockBuffer = freqBlock;

  const defs = useOne(() => ({
    USE_GRADIENT: gradient,
    SCORE_ONLY: false,
    SCORE_COMPARE: false,
    SWAP_GENERATE: false,
    SWAP_APPLY: false,
    SWAP_SPARSE: true,
    SWAP_WARMUP: false,
    SWAP_UNDO: false,
  }), gradient);

  const defs1 = useOne(() => ({
    ...defs,
    SWAP_GENERATE: true,
  }), defs);
  const defs2 = useOne(() => ({
    ...defs,
    SWAP_APPLY: true,
  }), defs);
  const defs3 = useOne(() => ({
    ...defs,
    SWAP_UNDO: true,
    SWAP_APPLY: true,
  }), defs);

  const gatherSize = Math.min(block * block * block, 256);

  const useScoreRegion = (slot: number, index: number) => {
    const sampleRegion = useDerivedSource(
      useBoundShader(swapSparse, [timeBuffer, s, bs, bo, index, swap], defs),
      {
          size: blockSize,
          length: blockLength,
      },
    );

    const meanShader = useBoundShader(swapMean, [bs, blockBuffer, mean]);
    const scoreShader = useBoundShader(swapScore, [bs, slot, targetFn.shader, blockBuffer, _dummy, mean, score], defs);

    return useMemo(() => [
      (slot === 0) ? use(FFT, {
        source: sampleRegion,
        target: blockCopy,
        skip: true,
        shouldDispatch: () => ((iter - 1) % batch) == 0,
      }) : null,
      use(FFT, {
        source: sampleRegion,
        target: blockBuffer,
      }),
      use(Dispatch, {
        size: [gatherSize, 1, 1],
        group: [gatherSize, 1, 1],
        shader: meanShader,
      }),
      use(Dispatch, {
        size: [gatherSize, 1, 1],
        group: [gatherSize, 1, 1],
        shader: scoreShader,
      }),
    ], [sampleRegion, blockCopy, blockBuffer, gatherSize, meanShader, scoreShader]);
  }

  const swapGenerateShader = useBoundShader(swapApply, [timeBuffer, gradBuffer, statsBuffer, b, i, l, s1, s2, score, swap], defs1);
  const swapMaybeShader = useBoundShader(swapApply, [timeBuffer, gradBuffer, statsBuffer, b, i, l, s1, s2, score, swap], defs2);
  const swapUndoShader = useBoundShader(swapApply, [timeBuffer, gradBuffer, statsBuffer, b, i, l, s1, s2, score, swap], defs3);

  const targetVersion = version + useVersion(targetFn);

  let iter = 0;

  const scoreRegion1A = useScoreRegion(0, 0);
  const scoreRegion2A = useScoreRegion(1, 1);
  const scoreRegion1B = useScoreRegion(2, 0);
  const scoreRegion2B = useScoreRegion(3, 1);

  const dispatch = useMemo(() => [
    yeet({
      compute: () => {
        clearBuffer(device, mean.buffer);
        clearBuffer(device, score.buffer);

        g.current = gradient;
        i.current  = iter;
        s1.current = Math.random();
        s2.current = Math.random();
        k.current = 1;
        iter++;
      },
    }),
    use(Dispatch, {
      size: [1, 1, 1],
      group: [1, 1, 1],
      shader: swapGenerateShader,
    }),
    scoreRegion1A,
    scoreRegion2A,
    use(Dispatch, {
      size: [1, 1, 1],
      group: [1, 1, 1],
      shader: swapMaybeShader,
    }),
    scoreRegion1B,
    scoreRegion2B,
    use(Dispatch, {
      size: [1, 1, 1],
      group: [1, 1, 1],
      shader: swapUndoShader,
    }),
    /*
    use(Readback, {
      source: swap,
      then: (swaps: number[]) => console.log('swap', swaps[0], swaps[1])
    }),
    use(Readback, {
      source: score,
      then: (scores: number[]) => {
        let last = (scores[0] + scores[1]) / 2;
        let current = (scores[2] + scores[3]) / 2;
        if (current < last) c++;
        t++;
        console.log('score', last, current, +(current < last));
    }}),
    */
  ], [
    device,
    size,
    mean,
    score,
    timeBuffer,
    freqBuffer,
    scoreRegion1A,
    scoreRegion2A,
    scoreRegion1B,
    scoreRegion2B,
    swapGenerateShader,
    swapMaybeShader,
    swapUndoShader,
    targetVersion,
    gradient,
  ]);

  return dispatch;
}, 'SwapSparse');
