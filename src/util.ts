export const isMobile = () => window.innerWidth < 768;
export const isNarrow = () => window.innerWidth < 500;

export const getPaddedSize = (width: number, height: number, radius: number) => {
  const paddedWidth = width + radius * 2;
  const paddedHeight = height + radius * 2;
  return [paddedWidth, paddedHeight] as [number, number];
};

export const π = Math.PI;
export const τ = Math.PI * 2;

export const lerp = (a: number, b: number, t: number) => a * (1 - t) + b * t;

export const clamp = (x: number, a: number, b: number) => Math.max(a, Math.min(b, x));

export const fmt = (x: number, s: number = 2) => {
  const d = x ? Math.max(0, Math.min(12, Math.ceil(-Math.log10(Math.abs(x))) + s)) : 1;
  return x.toFixed(d);
};

export const fmts = (x: number, s: number = 2) => {
  return x.toFixed(s);
};

export const iir = (k: number = 0.25, x: number = 0) => {
  let samples = Math.ceil(1/k);
  let int1 = 0;
  let int2 = 0;
  let avg = 0;
  let c = 0;

  return (x: number) => {
    if (c < samples) {
      int1 = int2 = avg = ((avg * c) + x) / (c + 1);
    }
    else {
      int1 = lerp(int1, x, k);
      int2 = lerp(int2, int1, k);
      avg = lerp(avg, int2, k);
    }
    c++;

    return avg;
  };
};

export const len3 = (x: number, y: number, z: number) => Math.sqrt(sqr(x) + sqr(y) + sqr(z));
export const len2 = (x: number, y: number) => Math.sqrt(sqr(x) + sqr(y));
export const sqr = (x: number) => x * x;

export const fmtTime = (time: number) => {
  const d = Math.floor((time) / 86400);
  const h = Math.floor((time - d * 86400) / 3600);
  const m = Math.floor((time - d * 86400 - h * 3600) / 60);
  const s = Math.floor((time - d * 86400 - h * 3600 - m * 60));

  const dd = pad2(d);
  const hh = pad2(h);
  const mm = pad2(m);
  const ss = pad2(s);
  const parts = [dd, hh, mm, ss];

  const i = parts.findIndex(s => s !== '00');
  if (i >= 0) return parts.slice(Math.min(i, 2)).join(':');
  return parts.join(':');
}

export const pad2 = (x: number) => ('00' + x).slice(-2);
