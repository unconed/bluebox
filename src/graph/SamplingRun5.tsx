import React, { LC, Provide } from '@use-gpu/live';

import { seq } from '@use-gpu/core';
import {
  LinearRGB, Pass, Flat, RawTexture, PanControls,
  CompositeData, LineLayer, LineSegments,
  useDeviceContext, useFontContext, DebugProvider,
  LayoutContext,
} from '@use-gpu/workbench';
import {
  UI, Layout, Absolute, Block, Inline, Text, Flex, Embed,
} from '@use-gpu/layout';
import {
  Sampled, Grid, Axis, Cartesian, Embedded, Point, Arrow, Line, Scale, Tick, Label,
  ValuesContext,
} from '@use-gpu/plot';

import { DATA_POS } from './data-pos';
import { DATA_MID } from './data-mid';
import { DATA_NEG } from './data-neg';

const SIGNALS = [
  DATA_POS.sort((a, b) => b.q - a.q).map((d, i) => d.dcdn),
  DATA_POS.sort((a, b) => b.dcdn - a.dcdn).map((d, i) => d.q),
  DATA_POS.sort((a, b) => b.q - a.q).map((d, i) => d.v - 0.97),
  DATA_POS.sort((a, b) => b.q - a.q).map((d, i) => d.dvdcdn),
];

const RANGES = [
  [-.35, .35],
  [-.01, .0012],
  [-0.05 - 2, 0.1],
  [-.5, .5],
];

const LABELS = [
  'Gradient by Quality',
  'Quality by Gradient',
  'Value difference by Quality',
  'Delta V × Gradient by Quality',
];

export const SamplingRun5 = ({index}: number) => {

  const n = DATA_POS.length;
  const signal = SIGNALS[index];
  const range = RANGES[index];
  const label = LABELS[index];

  const limit = SIGNALS[0];
  const ii = DATA_POS.findIndex(d => d.q < 0);

  const view = (
    <LinearRGB backgroundColor={[1, 1, 1, 1]}>
      <Flat>
        <Pass>
        
          <UI>
            <Layout>
              <Block width={'100%'} height={'100%'}>
                <Absolute right={0} top={10} width={300}>
                  <Block width={300} padding={[20, 10]} fill={[1, 1, 1, 1]}>
                    <Inline align="end"><Text family={"Lato"} weight={900} color={"#3090ff"}>{label}</Text></Inline>
                  </Block>
                </Absolute>
                <Embed width="100%" height="100%" margin={20}>
                  <Embedded normalize>

                    <Cartesian
                      range={[[0, n], range]}
                      scale={[1, -1, 1]}
                    >
                      <Grid color="#ccc" />
                      <Axis axis="y" origin={[0, 0, 0]} color="#aaa" width={2} />
                      <Axis axis="x" origin={[0, 0, 0]} color="#aaa" width={2} />
                      <Sampled
                        format={'vec2<f32>'}
                        index
                        size={[n]}
                        expr={(emit, x, i) => {
                          emit(i, signal[i]);
                        }}
                      >
                        <Point
                          color={"#3090ff"}
                          size={3}
                        />
                      </Sampled>


                      { index === 3 ? <Sampled
                        format={'vec2<f32>'}
                        index
                        size={[1]}
                        items={2}
                        expr={(emit, x, i) => {
                          emit(ii, range[0]);
                          emit(ii, range[1]);
                        }}
                      >
                        <Line
                          color={"#aaa"}
                          width={3}
                          zBias={-1}
                        />
                      </Sampled> : null}
                    </Cartesian>
            
                  </Embedded>
                </Embed>
              </Block>
            </Layout>
          </UI>

        </Pass>
      </Flat>
    </LinearRGB>
  );
  
  
  return view;
};
