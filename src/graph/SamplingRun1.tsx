import React, { LC, Provide } from '@use-gpu/live';

import { seq } from '@use-gpu/core';
import {
  LinearRGB, Pass, Flat, RawTexture, PanControls,
  CompositeData, LineLayer, LineSegments,
  useDeviceContext, useFontContext, DebugProvider,
  LayoutContext,
} from '@use-gpu/workbench';
import {
  UI, Layout, Absolute, Block, Inline, Text, Flex, Embed,
} from '@use-gpu/layout';
import {
  Sampled, Grid, Axis, Cartesian, Embedded, Point, Arrow, Line, Scale, Tick, Label,
  ValuesContext,
} from '@use-gpu/plot';

import { DATA } from './data-early';

const SIGNALS = [
  DATA.map((d, i) => d.max),
  DATA.map((d, i) => d.q),
  DATA.map((d, i) => d.maxQ),
];

export const SamplingRun1 = () => {

  const n = DATA.length;

  const view = (
    <LinearRGB backgroundColor={[1, 1, 1, 1]}>
      <Flat>
        <Pass>
        
          <UI>
            <Layout>
              <Block width={'100%'} height={'100%'}>
                <Absolute right={0} top={10} width={250}>
                  <Block width={250} padding={[20, 10]} fill={[1, 1, 1, 1]}>
                    <Inline align="end"><Text family={"Lato"} weight={900} color={"#3090ff"}>Max score improvement</Text></Inline>
                    <Inline align="end"><Text family={"Lato"} weight={900} color={"#40cf30"}>% of successful swaps</Text></Inline>
                    <Inline align="end"><Text family={"Lato"} weight={900} color={"#888"}>Quality</Text></Inline>
                  </Block>
                </Absolute>
                <Embed width="100%" height="100%" margin={20}>
                  <Embedded normalize>

                    <Cartesian
                      range={[[0, n], [0, 0.0006]]}
                      scale={[1, -1, 1]}
                    >
                      <Grid color="#ccc" />
                      <Sampled
                        format={'vec2<f32>'}
                        index
                        size={[n]}
                        expr={(emit, x, i) => {
                          emit(i, SIGNALS[0][i]);
                        }}
                      >
                        <Point
                          color={"#3090ff"}
                          size={3}
                        />
                      </Sampled>
                      <Sampled
                        format={'vec2<f32>'}
                        index
                        size={[n]}
                        expr={(emit, x, i) => {
                          emit(i, SIGNALS[1][i] * SIGNALS[0][0] / SIGNALS[1][0]);
                        }}
                      >
                        <Point
                          color={"#40cf30"}
                          size={3}
                        />
                      </Sampled>
                      <Sampled
                        format={'vec2<f32>'}
                        index
                        size={[n]}
                        expr={(emit, x, i) => {
                          emit(i, SIGNALS[2][i] * SIGNALS[0][0] / SIGNALS[2][0]);
                        }}
                      >
                        <Line
                          color={"#888"}
                          width={3}
                        />
                      </Sampled>
                    </Cartesian>
            
                  </Embedded>
                </Embed>
              </Block>
            </Layout>
          </UI>

        </Pass>
      </Flat>
    </LinearRGB>
  );
  
  
  return view;
};
