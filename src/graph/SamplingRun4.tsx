import React, { LC, Provide } from '@use-gpu/live';

import { seq } from '@use-gpu/core';
import {
  LinearRGB, Pass, Flat, RawTexture, PanControls,
  CompositeData, LineLayer, LineSegments,
  useDeviceContext, useFontContext, DebugProvider,
  LayoutContext,
} from '@use-gpu/workbench';
import {
  UI, Layout, Absolute, Block, Inline, Text, Flex, Embed,
} from '@use-gpu/layout';
import {
  Sampled, Grid, Axis, Cartesian, Embedded, Point, Arrow, Line, Scale, Tick, Label,
  ValuesContext,
} from '@use-gpu/plot';

import { DATA_POS } from './data-pos';
import { DATA_MID } from './data-mid';
import { DATA_NEG } from './data-neg';

const SIGNALS = [
  DATA_POS.map((d, i) => d.q),
  DATA_MID.map((d, i) => d.q),
  DATA_NEG.map((d, i) => d.q),
];

export const SamplingRun4 = ({index}: number) => {

  const n = DATA_POS.length;
  const signal = SIGNALS[index];

  const ii = signal.findIndex(d => d < 0);

  const view = (
    <LinearRGB backgroundColor={[1, 1, 1, 1]}>
      <Flat>
        <Pass>
        
          <UI>
            <Layout>
              <Block width={'100%'} height={'100%'}>
                <Absolute right={0} top={10} width={250}>
                  <Block width={250} padding={[20, 10]} fill={[1, 1, 1, 1]}>
                    <Inline align="end"><Text family={"Lato"} weight={900} color={"#3090ff"}>Quality</Text></Inline>
                  </Block>
                </Absolute>
                <Embed width="100%" height="100%" margin={20}>
                  <Embedded normalize>

                    <Cartesian
                      range={[[0, n], [-0.01, 0.0012]]}
                      scale={[1, -1, 1]}
                    >
                      <Grid color="#ccc" />
                      <Axis axis="y" origin={[0, 0, 0]} color="#aaa" width={2} />
                      <Axis axis="x" origin={[0, 0, 0]} color="#aaa" width={2} />
                      <Sampled
                        format={'vec2<f32>'}
                        index
                        size={[n]}
                        expr={(emit, x, i) => {
                          emit(i, signal[i]);
                        }}
                      >
                        <Line
                          color={"#3090ff"}
                          width={3}
                        />
                      </Sampled>
                      <Sampled
                        format={'vec2<f32>'}
                        index
                        size={[1]}
                        items={2}
                        expr={(emit, x, i) => {
                          emit(ii, -0.015);
                          emit(ii, 0.0012);
                        }}
                      >
                        <Line
                          color={"#aaa"}
                          width={3}
                          zBias={-1}
                        />
                      </Sampled>
                    </Cartesian>
            
                  </Embedded>
                </Embed>
              </Block>
            </Layout>
          </UI>

        </Pass>
      </Flat>
    </LinearRGB>
  );
  
  
  return view;
};
