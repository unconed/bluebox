import React, { LC, Provide } from '@use-gpu/live';

import { seq } from '@use-gpu/core';
import {
  LinearRGB, Pass, Flat, RawTexture, PanControls,
  CompositeData, LineLayer, LineSegments,
  useDeviceContext, useFontContext, DebugProvider,
  LayoutContext,
} from '@use-gpu/workbench';
import {
  UI, Layout, Absolute, Block, Inline, Text, Flex, Embed,
} from '@use-gpu/layout';
import {
  Sampled, Grid, Axis, Cartesian, Polar, Embedded, Point, Arrow, Label, Line, Scale, Tick,
  ValuesContext,
} from '@use-gpu/plot';

const π = Math.PI;
const τ = π * 2;

const phase = 0.33;
const band = 0.8;
const tgt = 0.5;

export const Gradient1 = () => {
  
  const view = (
    <LinearRGB backgroundColor={[1, 1, 1, 1]}>
      <Flat>
        <Pass>
        
          <UI>
            <Layout>
              <Block width={'100%'} height={'100%'}>
                <Embed width="100%" height="100%" margin={0}>
                  <Embedded normalize>

                    <Polar
                      bend={1}
                      range={[[-π, π], [0, 1]]}
                      scale={[1, -1, 1]}
                    >
                      <Grid
                        first={{base: 2, unit: π, divide: 4}}
                        second={{detail: 64}}
                        color="#c0c0c0"
                      />
                      <Sampled
                        format={'vec2<f32>'}
                        index
                        size={[256]}
                        items={2}
                        expr={(emit, x, i) => {
                          emit(x, band);
                        }}
                      >
                        <Line
                          color={"#3090ff"}
                          width={3}
                        />
                      </Sampled>
                      <Sampled
                        format={'vec2<f32>'}
                        index
                        size={[128]}
                        items={2}
                        expr={(emit, x, i) => {
                          emit(x, tgt);
                        }}
                      >
                        <Line
                          color={"#000"}
                          width={3}
                        />
                      </Sampled>
                      <Sampled
                        format={'vec2<f32>'}
                        index
                        size={[2]}
                        items={2}
                        expr={(emit, x, i) => {
                          emit(0, 0);
                          emit(phase, band);
                        }}
                      >
                        <Provide context={ValuesContext} value={[0, 0]}>
                          <Provide context={LayoutContext} value={[0, 0, 10, -10]}>
                            <Label
                              labels={["", "Amplitude"]}
                              color={"#3090ff"}
                              size={20}
                              weight={900}
                              offset={35}
                              placement={"topLeft"}
                            />
                          </Provide>
                        </Provide>
                        <Arrow
                          color={"#3090ff"}
                          width={5}
                        />
                      </Sampled>

                      <Sampled
                        format={'vec2<f32>'}
                        index
                        size={[2]}
                        items={2}
                        expr={(emit, x, i) => {
                          emit(phase, tgt);
                        }}
                      >
                        <Provide context={ValuesContext} value={[0, 0]}>
                          <Provide context={LayoutContext} value={[0, 0, 10, -10]}>
                            <Label
                              labels={["Target"]}
                              color={"#000000"}
                              size={20}
                              weight={900}
                              offset={75}
                              placement={"left"}
                            />
                          </Provide>
                        </Provide>
                      </Sampled>
                    </Polar>
            
                  </Embedded>
                </Embed>
              </Block>
            </Layout>
          </UI>

        </Pass>
      </Flat>
    </LinearRGB>
  );
  
  
  return view;
};
