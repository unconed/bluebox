import React, { LC, Provide } from '@use-gpu/live';

import { seq } from '@use-gpu/core';
import {
  LinearRGB, Pass, Flat, RawTexture, PanControls,
  CompositeData, LineLayer, LineSegments,
  useDeviceContext, useFontContext, DebugProvider,
  LayoutContext,
} from '@use-gpu/workbench';
import {
  UI, Layout, Absolute, Block, Inline, Text, Flex, Embed,
} from '@use-gpu/layout';
import {
  Sampled, Grid, Axis, Cartesian, Polar, Embedded, Point, Arrow, Label, Line, Scale, Tick,
  ValuesContext,
} from '@use-gpu/plot';

const π = Math.PI;
const τ = π * 2;

const phase = 0.33;
const band = 0.8;
const tgt = 0.5;

const action = 2.3;
const mask = 0.5;

const dx = Math.cos(action) * mask;
const dy = Math.sin(action) * mask;

const c = Math.cos(phase);
const s = Math.sin(phase);

const xi = c * band;
const yi = s * band;

const xo = xi + dx;
const yo = yi + dy;

const shiftPhase = Math.atan2(yo, xo);
const shiftBand = Math.sqrt(xo*xo + yo*yo);

const l1 = c * dx + s * dy;
const l2 = c * dy - s * dx;

const xt = xi - s * l2;
const yt = yi + c * l2;

const tanPhase = Math.atan2(yt, xt);
const tanBand = Math.sqrt(xt*xt + yt*yt);

export const Gradient2 = () => {
  
  const view = (
    <LinearRGB backgroundColor={[1, 1, 1, 1]}>
      <Flat>
        <Pass>
        
          <UI>
            <Layout>
              <Block width={'100%'} height={'100%'}>
                <Embed width="100%" height="100%" margin={0}>
                  <Embedded normalize>

                    <Polar
                      bend={1}
                      range={[[-π, π], [0, 1]]}
                      scale={[2, -2, 1]}
                      position={[-0.9, 0.9, 0]}
                    >
                      <Grid
                        first={{base: 2, unit: π, divide: 4}}
                        second={{detail: 64}}
                        color="#c0c0c0"
                      />
                      <Sampled
                        format={'vec2<f32>'}
                        index
                        size={[256]}
                        items={2}
                        expr={(emit, x, i) => {
                          emit(x, band);
                        }}
                      >
                        <Line
                          color={"#3090ff"}
                          width={3}
                        />
                      </Sampled>
                      <Sampled
                        format={'vec2<f32>'}
                        index
                        size={[128]}
                        items={2}
                        expr={(emit, x, i) => {
                          emit(x, tgt);
                        }}
                      >
                        <Line
                          color={"#000"}
                          width={3}
                        />
                      </Sampled>
                      <Sampled
                        format={'vec2<f32>'}
                        index
                        size={[2]}
                        items={2}
                        expr={(emit, x, i) => {
                          emit(phase, band);
                          emit(shiftPhase, shiftBand);
                        }}
                      >
                        <Arrow
                          color={"#c03f90"}
                          width={5}
                        />
                      </Sampled>
                      <Sampled
                        format={'vec2<f32>'}
                        index
                        size={[2]}
                        items={2}
                        expr={(emit, x, i) => {
                          emit(phase, band);
                          emit(phase, band + l1);
                        }}
                      >
                        <Provide context={ValuesContext} value={[0, 0]}>
                          <Provide context={LayoutContext} value={[0, 0, 10, -10]}>
                            <Label
                              labels={["", "Radial"]}
                              color={"#000"}
                              size={20}
                              weight={900}
                              offset={20}
                              placement={"bottomLeft"}
                            />
                          </Provide>
                        </Provide>
                        <Arrow
                          color={"#000"}
                          width={5}
                          zBias={1}
                        />
                      </Sampled>
                      <Sampled
                        format={'vec2<f32>'}
                        index
                        size={[2]}
                        items={2}
                        expr={(emit, x, i) => {
                          emit(phase, band);
                          emit(tanPhase, tanBand);
                        }}
                      >
                        <Provide context={ValuesContext} value={[0, 0]}>
                          <Provide context={LayoutContext} value={[0, 0, 10, -10]}>
                            <Label
                              labels={["", "Tangential"]}
                              color={"#3090f0"}
                              size={20}
                              weight={900}
                              offset={0}
                              placement={"bottomLeft"}
                            />
                          </Provide>
                        </Provide>
                        <Arrow
                          color={"#3090f0"}
                          width={5}
                          zBias={1}
                        />
                      </Sampled>
                      <Sampled
                        format={'vec2<f32>'}
                        index
                        size={[2]}
                        items={2}
                        expr={(emit, x, i) => {
                          emit(0, 0);
                          emit(phase, band);
                        }}
                      >
                        <Line
                          color={"#3090ff"}
                          width={3}
                        />
                      </Sampled>

                      <Sampled
                        format={'vec2<f32>'}
                        index
                        size={[2]}
                        items={2}
                        expr={(emit, x, i) => {
                          emit(shiftPhase, shiftBand);
                        }}
                      >
                        <Provide context={ValuesContext} value={[0, 0]}>
                          <Provide context={LayoutContext} value={[0, 0, 10, -10]}>
                            <Label
                              labels={["Interference\nmask"]}
                              color={"#c03f90"}
                              size={20}
                              weight={900}
                              offset={0}
                              placement={"bottomRight"}
                            />
                          </Provide>
                        </Provide>
                      </Sampled>
                    </Polar>
            
                  </Embedded>
                </Embed>
              </Block>
            </Layout>
          </UI>

        </Pass>
      </Flat>
    </LinearRGB>
  );
  
  
  return view;
};
