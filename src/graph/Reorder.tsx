import React, { LC } from '@use-gpu/live';

import { seq } from '@use-gpu/core';
import {
  LinearRGB, Pass, Flat, RawTexture, PanControls,
  CompositeData, LineLayer, LineSegments,
  useDeviceContext, useFontContext, DebugProvider,
} from '@use-gpu/workbench';
import {
  UI, Layout, Absolute, Block, Inline, Text, Flex, Embed,
} from '@use-gpu/layout';
import {
  Sampled, Grid, Axis, Cartesian, Embedded, Point, Arrow, Line, Scale, Tick,
} from '@use-gpu/plot';

import { scrambleBits53, mixBits53 } from '@use-gpu/state';

const makeRandom = (seed: number) => {
  return () => {
    seed = mixBits53(seed, 1231);
    const value = scrambleBits53(seed) / (0x100000000 * 0x200000);
    return value;
  };
};

const DETAIL = 128;

const random = makeRandom(0x12345);

const SIGNAL = seq(DETAIL).map(() => random());

const SIGNAL0 = SIGNAL.slice();
const SIGNAL1 = SIGNAL.slice().sort((a, b) => a - b);
const SIGNAL2 = seq(DETAIL).map(i => i < 64 ? SIGNAL1[i * 2] : SIGNAL1[(i - 64) * 2 + 1]);
const SIGNAL3 = seq(DETAIL).map(i => i < 64 ? SIGNAL2[i * 2] : SIGNAL2[(i - 64) * 2 + 1]);

export const Reorder = ({index}: number) => {
  
  const signal = [SIGNAL0, SIGNAL1, SIGNAL2, SIGNAL3][index];
  
  const view = (
    <LinearRGB backgroundColor={[1, 1, 1, 1]}>
      <Flat>
        <Pass>
        
          <UI>
            <Layout>
              <Block width={'100%'} height={'100%'}>
                <Embed width="100%" height="100%" margin={20}>
                  <Embedded normalize>

                    <Cartesian
                      range={[[0, DETAIL], [0, 1]]}
                      scale={[1, -1, 1]}
                    >
                      <Grid />
                      <Sampled
                        format={'vec2<f32>'}
                        index
                        size={[DETAIL]}
                        expr={(emit, x, i) => {
                          emit(i, signal[i]);
                        }}
                      >
                        <Line
                          color={"#3090ff"}
                          width={3}
                        />
                      </Sampled>
                    </Cartesian>
            
                  </Embedded>
                </Embed>
              </Block>
            </Layout>
          </UI>

        </Pass>
      </Flat>
    </LinearRGB>
  );
  
  
  return view;
};

export const Reorder1 = () => {
  return <Reorder index={0} />;
};

export const Reorder2 = () => {
  return <Reorder index={1} />;
};

export const Reorder3 = () => {
  return <Reorder index={2} />;
};

export const Reorder4 = () => {
  return <Reorder index={3} />;
};