import React, { LC, Provide } from '@use-gpu/live';

import { seq } from '@use-gpu/core';
import {
  LinearRGB, Pass, Flat, RawTexture, PanControls,
  CompositeData, LineLayer, LineSegments,
  useDeviceContext, useFontContext, DebugProvider,
  LayoutContext,
} from '@use-gpu/workbench';
import {
  UI, Layout, Absolute, Block, Inline, Text, Flex, Embed,
} from '@use-gpu/layout';
import {
  Sampled, Grid, Axis, Cartesian, Embedded, Point, Arrow, Line, Scale, Tick, Label,
  ValuesContext,
} from '@use-gpu/plot';

import { DATA } from './data-early';

const SIGNALS = [
  DATA.slice().sort((a, b) => b.maxQ - a.maxQ).map((d, i) => Math.abs(d.dcdn)),
];

export const SamplingRun3 = () => {

  const n = DATA.length;

  const view = (
    <LinearRGB backgroundColor={[1, 1, 1, 1]}>
      <Flat>
        <Pass>
        
          <UI>
            <Layout>
              <Block width={'100%'} height={'100%'}>
                <Absolute right={0} top={10} width={250}>
                  <Block width={250} padding={[20, 10]} fill={[1, 1, 1, 1]}>
                    <Inline align="end"><Text family={"Lato"} weight={900} color={"#3090ff"}>Gradient by Quality</Text></Inline>
                  </Block>
                </Absolute>
                <Embed width="100%" height="100%" margin={20}>
                  <Embedded normalize>

                    <Cartesian
                      range={[[0, n], [0, 0.35]]}
                      scale={[1, -1, 1]}
                    >
                      <Grid color="#ccc" />
                      <Axis axis="y" origin={[0, 0, 0]} color="#aaa" width={2} />
                      <Axis axis="x" origin={[0, 0, 0]} color="#aaa" width={2} />
                      <Sampled
                        format={'vec2<f32>'}
                        index
                        size={[n]}
                        expr={(emit, x, i) => {
                          emit(i, SIGNALS[0][i]);
                        }}
                      >
                        <Line
                          color={"#3090ff"}
                          size={3}
                        />
                      </Sampled>
                    </Cartesian>
            
                  </Embedded>
                </Embed>
              </Block>
            </Layout>
          </UI>

        </Pass>
      </Flat>
    </LinearRGB>
  );
  
  
  return view;
};
